from django.shortcuts import render
from .models import Post, Tag
from datetime import datetime


def index(request):
    posts_list = Post.objects.filter(pub_date_time__lte=datetime.now())
    tags_list = Tag.objects.all()
    context = {
        'posts_list': posts_list,
        'tags_list': tags_list,
    }
    return render(request, 'news/posts.html', context)


def post(request, slug):
    c_post = Post.objects.get(slug=slug)
    context = {
        'c_post': c_post,
    }
    return render(request, 'news/post.html', context)


def tag(request, slug):
    posts_list = Post.objects.filter(tags__slug=slug)
    tag = Tag.objects.get(slug=slug)
    tags_list = Tag.objects.all()
    context = {
        'posts_list': posts_list,
        'tag': tag,
        'tags_list': tags_list,
    }
    return render(request, 'news/posts.html', context)
