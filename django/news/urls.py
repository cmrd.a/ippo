from django.urls import path

from . import views

app_name = 'news'

urlpatterns = [
    path('', views.index, name="index"),
    path('<str:slug>', views.post, name="post"),
    path('tag/<str:slug>', views.tag, name="tag"),
]
