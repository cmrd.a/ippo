from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.utils.text import slugify
from django.utils.timezone import now

from main.models import my_slugify


class Tag(models.Model):
    name = models.CharField(max_length=45, unique=True, verbose_name='Тег', help_text='Максимум 45 символов')
    slug = models.CharField(max_length=90, verbose_name='Ссылка', blank=True, unique=True,
                            help_text='Сгенерируется после первого сохранения. Потом можно менять вручную')

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = my_slugify(self.name)
        super(Tag, self).save(*args, **kwargs)


class Post(models.Model):
    title = models.CharField(max_length=256, verbose_name='Заголовок')
    content = RichTextUploadingField(verbose_name='Содержимое', blank=True)
    pub_date_time = models.DateTimeField(verbose_name='Дата и время публикации', default=now,
                                         help_text='Новость не будет видно, пока не наступит указанное время')
    tags = models.ManyToManyField(Tag, blank=True, verbose_name='Теги')
    slug = models.CharField(max_length=256, verbose_name='Ссылка', blank=True, unique=True,
                            help_text='Сгенерируется после первого сохранения. Потом можно менять вручную')

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"
        ordering = ['-pub_date_time']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.pub_date_time.date()) + '_' + my_slugify(self.title)
        super(Post, self).save(*args, **kwargs)
