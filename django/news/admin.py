from django.contrib import admin
from .models import Post, Tag


class TagAdmin(admin.ModelAdmin):
    search_fields = ['name']


class PostAdmin(admin.ModelAdmin):
    list_filter = ['tags']
    search_fields = ['title', 'content']
    # exclude = ('tag', )
    autocomplete_fields = ['tags']


admin.site.register(Post, PostAdmin)
admin.site.register(Tag, TagAdmin)
