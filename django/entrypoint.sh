#!/usr/bin/env bash
mkdir -p /opt/ippo/django/main/static/main/npm/js
mkdir -p /opt/ippo/django/main/static/main/npm/css

until cp /opt/ippo/node/node_modules/jquery/dist/jquery.min.js /opt/ippo/django/main/static/main/npm/js/jquery.min.js; do
  sleep 10
  echo "Retry!";
done

until cp -r /opt/ippo/node/node_modules/@fortawesome/fontawesome-free/* /opt/ippo/django/main/static/main/npm/; do
  sleep 10
  echo "Retry!";
done

until cp -r /opt/ippo/node/node_modules/bootstrap/dist/* /opt/ippo/django/main/static/main/npm/; do
  sleep 10
  echo "Retry!";
done

until cp /opt/ippo/node/node_modules/selectize/dist/js/standalone/selectize.min.js /opt/ippo/django/main/static/main/npm/js/selectize.min.js; do
  sleep 10
  echo "Retry!";
done

until cp /opt/ippo/node/node_modules/selectize/dist/css/selectize.default.css /opt/ippo/django/main/static/main/npm/css/selectize.default.css; do
  sleep 10
  echo "Retry!";
done


#python manage.py migrate --no-input
#python manage.py collectstatic --no-input --clear
python manage.py collectstatic --no-input

#mkdir -p /opt/ippo/media
#chown -R nobody:nogroup /opt/ippo/media
#chmod -R 660 /opt/ippo/media
#chmod -R g+s /opt/ippo/media


#gunicorn ippo.wsgi -w 2 -b 0.0.0.0 --reload --capture-output --log-level debug --enable-stdio-inheritance
#gunicorn ippo.wsgi -c gunicorn_conf.py
python manage.py runserver 0.0.0.0:9000