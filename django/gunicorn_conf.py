from os import environ
from multiprocessing import cpu_count
import glob

if not environ.get('DEBUG'):
    workers = cpu_count()
    loglevel = '-'
else:
    workers = 1
    capture_output = True
    enable_stdio_inheritance = True
    loglevel = 'debug'
    path = '/opt/ippo/django/'
    templates = [f for f in glob.glob(path + "**/*.html", recursive=True)]
    reload_extra_files = templates

bind = '0.0.0.0:8000'
reload = True
# loglevel = 'debug'

# accesslog = "-" #'-' means log to stdout.
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'
