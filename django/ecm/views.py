import os

from django.conf import settings
from django.http import HttpResponse, HttpResponseForbidden

from .models import Document


def protected_view(request, path):
    print(path)
    access_granted = False

    user = request.user
    if user.is_authenticated:
        if user.is_staff:
            access_granted = True
        else:
            if user.has_perm('ecm.view_document'):
                access_granted = True

    if access_granted:
        response = HttpResponse()
        del response['Content-Type']
        response["X-Accel-Redirect"] = os.path.join(settings.PROTECTED_MEDIA_LOCATION_PREFIX, 'ecm', path).encode(
            'utf-8')
        print(response["X-Accel-Redirect"])
        # response['X-Accel-Redirect'] = os.path.join('/internal/' + path).encode('utf-8')
        return response
    else:
        return HttpResponseForbidden('Not authorized to access this media.')
