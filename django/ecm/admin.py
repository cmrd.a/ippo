from django.contrib import admin
from django.contrib import messages
from .models import Document, Category


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['category']


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('code', 'from_whom', 'creation_date', 'register_date')
    readonly_fields = ('code',)
    list_filter = ('direction', 'to_whom', 'from_whom', 'category',)
    search_fields = ('code', 'to_whom__last_name', 'from_whom__last_name', 'executor__last_name')
    autocomplete_fields = ('category', 'to_whom', 'from_whom', 'executor')


admin.site.register(Category, CategoryAdmin)
admin.site.register(Document, DocumentAdmin)

# def save_model(self, request, obj, form, change):
# 	if obj.id:
# 		super(DocumentAdmin, self).save_model(request, obj, form, change)
# 	else:
# 		if obj.pdf_file or obj.docx_file:
# 			messages.add_message(request, messages.WARNING, 'Первое сохранение происходит без файов')
# 		super(DocumentAdmin, self).save_model(request, obj, form, change)
