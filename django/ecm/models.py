from django.core.validators import FileExtensionValidator
from django.db import models
from django.utils.timezone import now

from main import storage
from people.models import Worker


class Category(models.Model):
    name = models.CharField(max_length=128, unique=True, verbose_name='Название')

    class Meta:
        verbose_name = "Тип"
        verbose_name_plural = "Типы"
        ordering = ['name']

    def __str__(self):
        return self.name


class Document(models.Model):
    DIRECTION_CHOICES = [('вхд', 'Входящий'), ('исх', 'Исходящий'), ('внт', 'Внутренний')]
    direction = models.CharField(choices=DIRECTION_CHOICES, max_length=3, verbose_name='Вид')
    category = models.ForeignKey(Category, models.CASCADE, verbose_name='Тип')
    creation_date = models.DateField(default=now, verbose_name='Дата создания')
    register_date = models.DateField(null=True, blank=True, verbose_name='Дата регистрации')
    to_whom = models.ManyToManyField(Worker, related_name='recipient', verbose_name='Кому')
    from_whom = models.ForeignKey(Worker, on_delete=models.SET_NULL, null=True, related_name='initiator',
                                  verbose_name='От кого')
    executor = models.ForeignKey(Worker, on_delete=models.SET_NULL, null=True, blank=True, related_name='executor',
                                 verbose_name='Исполнитель')
    pdf_file = models.FileField(upload_to=storage.upload_ecm_document, storage=storage.ProtectedFileSystemStorage(),
                                validators=[FileExtensionValidator(allowed_extensions=['pdf'])], null=True, blank=True,
                                verbose_name='PDF')
    docx_file = models.FileField(upload_to=storage.upload_ecm_document, storage=storage.ProtectedFileSystemStorage(),
                                 validators=[FileExtensionValidator(allowed_extensions=['docx'])], null=True,
                                 blank=True, verbose_name='DOCX')
    code = models.CharField(max_length=32, unique_for_month=True, verbose_name='Код', blank=True)

    class Meta:
        verbose_name = "Документ"
        verbose_name_plural = "Документы"
        ordering = ['-creation_date']

    def __str__(self):
        return f'{self.get_direction_display()} {self.category} {self.creation_date}'

    def save(self, *args, **kwargs):
        temp_pdf_file = self.pdf_file
        temp_docx_file = self.docx_file
        if not self.id:
            self.pdf_file = None
            self.docx_file = None
            super(Document, self).save(*args, **kwargs)
            code = self.direction + str(self.creation_date.strftime("%y%m"))

            # map_dir = enumerate(self.DIRECTION_CHOICES, 1)
            # for m in map_dir:
            #     if self.direction in m[1][0]:
            #         code += str(m[0])

            suffix = 0
            while Document.objects.filter(code=code + str(suffix).zfill(2)):
                suffix += 1

            self.code = code + str(suffix).zfill(2)
            super(Document, self).save(*args, **kwargs)
            self.pdf_file = temp_pdf_file
            self.docx_file = temp_docx_file
            super(Document, self).save(*args, **kwargs)
        else:
            super(Document, self).save(*args, **kwargs)
