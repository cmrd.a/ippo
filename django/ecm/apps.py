from django.apps import AppConfig


class EcmConfig(AppConfig):
    name = 'ecm'
    verbose_name = 'Служебки и т.д.'
