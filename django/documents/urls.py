from django.urls import path

from . import views

app_name = 'documents'
urlpatterns = [
    path('', views.index, name="index"),
    path('category/<int:category_id>', views.check_category_permissions, name="check_category_permissions"),
    path('category/<int:category_id>/get_category_qr', views.get_category_qr, name="get_category_qr"),
    path('category/<int:document_id>/get_document_qr', views.get_document_qr, name="get_document_qr"),
]
