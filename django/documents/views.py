import os
import mimetypes

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import Document, Category
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.contrib.sites.shortcuts import get_current_site


def index(request):
    context = {
        'categories': Category.objects.all(),
    }
    return render(request, 'documents/index.html', context)


def check_category_permissions(request, category_id):
    category = Category.objects.get(id=category_id)
    if not category.public and not request.user.has_perm('documents.change_category'):
        raise Http404
    else:
        context = {
            'documents': Document.objects.filter(category_id=category_id),
            'category': Category.objects.get(id=category_id),
        }
        return render(request, 'documents/category.html', context)


def get_category_qr(request, category_id):
    category = Category.objects.get(id=category_id)
    current_site = str(get_current_site(request))
    host = str(current_site + '/documents/category/')
    image = category.get_qr(host)
    response = HttpResponse(content_type='image/png')
    image.save(response, "PNG")
    return response


def get_document_qr(request, document_id):
    document = Document.objects.get(id=document_id)
    current_site = str(get_current_site(request)) + '/'
    image = document.get_qr(current_site)
    print(request.get_host())
    response = HttpResponse(content_type='image/png')
    image.save(response, "PNG")
    return response


def protected_view(request, path):
    access_granted = False

    document = Document.objects.get(file=path)
    if document.category.public:
        access_granted = True
    else:
        user = request.user
        if user.is_authenticated:
            if user.is_staff:
                access_granted = True
            else:
                if user.has_perm('documents.change_category'):
                    access_granted = True

    if access_granted:
        response = HttpResponse()
        del response['Content-Type']
        response["X-Accel-Redirect"] = os.path.join(settings.PROTECTED_MEDIA_LOCATION_PREFIX, path).encode('utf-8')
        print(response["X-Accel-Redirect"])
        # response['X-Accel-Redirect'] = os.path.join('/internal/' + path).encode('utf-8')
        return response
    else:
        return HttpResponseForbidden('Not authorized to access this media.')
