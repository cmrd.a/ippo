import datetime

import qrcode
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

from main.storage import upload_document, ProtectedFileSystemStorage


class Category(MPTTModel):
    public = models.BooleanField(default=True, verbose_name='Публичная')
    name = models.CharField(max_length=256, verbose_name='Название')
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True,
                            related_name='children', verbose_name='Родительская категория')
    description = RichTextUploadingField(null=True, blank=True, default='', verbose_name='Описание')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        unique_together = ['parent', 'name']

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return f'{self.name} #{self.id}'

    def get_all_documents(self):
        return Document.objects.filter(category__in=self.get_descendants())

    def get_qr(self, host):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=1,
        )
        qr.add_data(host + str(self.id))
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        return img


class Document(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория')
    name = models.CharField(max_length=512, verbose_name='Название')
    description = RichTextUploadingField(null=True, blank=True, default='', verbose_name='Описание')
    file = models.FileField(max_length=1024, upload_to=upload_document, storage=ProtectedFileSystemStorage(), null=True,
                            blank=True,
                            verbose_name='Файл')
    url = models.URLField(null=True, blank=True, default=None, verbose_name='Ссылка')
    created = models.DateField(editable=False, verbose_name='Создано')
    updated = models.DateField(editable=False, verbose_name='Обновлено')

    class Meta:
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'
        ordering = ['name']
        unique_together = ['category', 'name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = datetime.date.today()
        self.updated = datetime.date.today()
        super(Document, self).save(*args, **kwargs)

    def get_content(self):
        url = 'nothing'
        try:
            url = self.file.url
        except ValueError:
            pass
        if self.url:
            url = self.url
        return url

    def get_qr(self, host):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=1,
        )
        qr.add_data(str(host + str(self.file.url)).encode('utf-8'))
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        return img
