from django.contrib import admin
from .models import Category, Document
from mptt.admin import MPTTModelAdmin
from mptt.admin import TreeRelatedFieldListFilter


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'updated',)
    search_fields = ('name', 'category__name',)
    list_filter = (
        ('category', TreeRelatedFieldListFilter),
    )


class DocumentInline(admin.StackedInline):
    model = Document
    extra = 0
    show_change_link = True


class CustomMPTTModelAdmin(MPTTModelAdmin):
    mptt_level_indent = 30
    inlines = [DocumentInline, ]


admin.site.register(Category, CustomMPTTModelAdmin)
admin.site.register(Document, DocumentAdmin)
