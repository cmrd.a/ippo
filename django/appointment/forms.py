from dal import autocomplete
from django import forms

from people.models import Student, Tutor, Teacher, Group
from .models import QueueRecord, Appointment


class RecordForm(forms.Form):
    last_name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={'class': 'form-control'}))
    group = forms.ModelChoiceField(queryset=Group.objects.all(),
                                   widget=autocomplete.ModelSelect2(url='appointment:group-autocomplete'))


class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ['available', 'teachers', 'tutors', 'start', 'finish', 'interval', 'max', 'instruction',
                  'categories', 'subjects']
        widgets = {
            'available': forms.CheckboxInput(),
            'teachers': forms.SelectMultiple(attrs={'class': 'form-control'}),
            'tutors': forms.SelectMultiple(attrs={'class': 'form-control'}),
            'start': forms.DateTimeInput(attrs={'class': 'form-control'}),
            'finish': forms.TimeInput(attrs={'class': 'form-control'}),
            'interval': forms.TimeInput(attrs={'class': 'form-control'}),
            'max': forms.NumberInput(attrs={'class': 'form-control', 'min': 0, 'max': 1024}),
            'instruction': forms.Textarea(attrs={'class': 'form-control', 'rows': 10}),
            'categories': forms.SelectMultiple(attrs={'class': 'form-control'}),
            'subjects': forms.SelectMultiple(attrs={'class': 'form-control'})
        }


class QueueForm(forms.ModelForm):
    class Meta:
        model = QueueRecord
        fields = ['category', 'subject', 'student', 'time', 'number', 'mark', ]
        widgets = {
            'category': forms.Select(attrs={'class': 'form-control'}),
            'subject': forms.Select(attrs={'class': 'form-control'}),
            'student': forms.Select(attrs={'class': 'form-control'}),
            'mark': forms.Select(attrs={'class': 'form-control'}),
        }
