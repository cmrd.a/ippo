import datetime

import xlsxwriter
from dal import autocomplete
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect

from people.models import Student, Group
from .forms import RecordForm, AppointmentForm, QueueForm
from .models import QueueRecord, Subject, Category, Appointment


def index(request):
    context = {
        'appointments': Appointment.objects.filter(available=True).order_by('start')
    }
    return render(request, 'appointment/index.html', context)


def appointment_details(request, appointment_id):
    appointment = get_object_or_404(Appointment, pk=appointment_id)
    if request.method == 'POST':
        if appointment.get_left() < 1:
            return HttpResponseRedirect(f'/appointments/{appointment_id}/result/full/')
        form = RecordForm(request.POST)
        if form.is_valid():
            last_name = form.cleaned_data.get('last_name')
            first_name = form.cleaned_data.get('first_name')
            group = form.cleaned_data.get('group')
            category = Category.objects.get(name=request.POST['category'])
            subject = Subject.objects.get(name=request.POST['subject'])

            student, created = Student.objects.get_or_create(group=group, last_name=last_name, first_name=first_name)

            try:
                qtime = datetime.datetime.strptime(request.POST['time'], '%H:%M').time()
            except KeyError:
                qtime = datetime.time(0, 0, 0)
            try:
                QueueRecord.objects.get(student=student, appointment=appointment, category=category, subject=subject)
                return HttpResponseRedirect(f'/appointments/{appointment_id}/result/repeatedly/')
            except QueueRecord.DoesNotExist:
                recorded = QueueRecord.objects.filter(appointment=appointment_id).count()
                new_queue = QueueRecord(appointment=appointment, category=category, subject=subject, student=student,
                                        time=qtime, number=recorded + 1)
                if (qtime in QueueRecord.objects.filter(appointment=appointment_id).values_list('time', flat=True)
                        and appointment.get_app_type() == 'interval'):
                    return HttpResponseRedirect(f'/appointments/{appointment_id}/result/taked/')
                new_queue.remote_address = request.META.get('HTTP_X_FORWARDED_FOR')
                new_queue.save()
            return HttpResponseRedirect(f'/appointments/{appointment_id}/result/success/')
    else:
        form = RecordForm()
    context = {
        'appointment': appointment,
        'form': form,
        'categories': appointment.categories,
        'subjects': appointment.subjects,
        'appointment_id': appointment_id,
    }
    return render(request, 'appointment/appointment_details.html', context)


def result(request, appointment_id, decision):
    appointment = get_object_or_404(Appointment, pk=appointment_id)
    context = {
        'appointment': appointment,
        'decision': decision,
    }
    return render(request, 'appointment/result.html', context)


@login_required()
def edit_appointment(request, appointment_id=None):
    if appointment_id:
        appointment = get_object_or_404(Appointment, pk=appointment_id)
    else:
        appointment = Appointment()

    if request.method == 'POST':
        form = AppointmentForm(request.POST or None, instance=appointment)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/appointments')
    else:
        form = AppointmentForm(instance=appointment)
        context = {
            'form': form,
            'appointment_id': appointment_id,
            'appointment': appointment,
        }
        return render(request, 'appointment/edit_appointment.html', context)


@login_required()
def delete_appointment(request, appointment_id):
    appointment = get_object_or_404(Appointment, pk=appointment_id)
    appointment.delete()
    return redirect('appointments:index')


@login_required()
def queue_list(request, appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    queues = QueueRecord.objects.filter(appointment=appointment)
    form_list = []
    for q in queues:
        form = QueueForm(instance=q)
        form_list.append(form)
    context = {
        'appointment': appointment,
        'queue_list': queues,
        'form_list': form_list,
    }
    return render(request, 'appointment/queue_list.html', context)


@login_required()
def delete_queue_record(request, appointment_id, queue_id):
    queue = get_object_or_404(QueueRecord, id=queue_id)
    queue.delete()
    return HttpResponseRedirect(f'/appointments/{appointment_id}/queue_list/')


@login_required()
def queue_list_export(request, appointment_id):
    appointment = get_object_or_404(Appointment, id=appointment_id)

    response = HttpResponse(content_type='application/xlsx')
    response['Content-Disposition'] = 'attachment; filename=queue_list_' \
                                      + f'{appointment.start.strftime("%y%m%d")}_{appointment_id}.xlsx'
    workbook = xlsxwriter.Workbook(response)
    worksheet = workbook.add_worksheet("Лист1")
    title = workbook.add_format({
        'bold': True,
        'font_size': 14,
        'align': 'center',
        'valign': 'vcenter'
    })
    worksheet.merge_range('A1:J1', appointment.__str__(), title)
    header = workbook.add_format({
        'bg_color': '#F7F7F7',
        'color': 'black',
        'align': 'center',
        'valign': 'top',
        'border': 1
    })
    worksheet.write(4, 0, '№', header)
    worksheet.write(4, 1, QueueRecord._meta.get_field("student").verbose_name, header)
    worksheet.write(4, 2, Student._meta.get_field("group").verbose_name, header)
    worksheet.write(4, 3, QueueRecord._meta.get_field("created_time").verbose_name, header)
    worksheet.write(4, 4, QueueRecord._meta.get_field("mark").verbose_name, header)

    queue_list = QueueRecord.objects.filter(appointment=appointment)
    for idx, obj in enumerate(queue_list):
        row = 5 + idx
        worksheet.write_number(row, 0, idx + 1)
        worksheet.write_string(row, 1, f'{obj.student.last_name} {obj.student.first_name} {obj.student.middle_name}')
        worksheet.write_string(row, 2, f'{obj.student.group}')
        worksheet.write(row, 3, f'{obj.created_time.strftime("%d.%m.%y %H:%M")}')
    worksheet.set_column('B:B', 40)
    worksheet.set_column('C:C', 12)
    worksheet.set_column('D:D', 20)

    workbook.close()
    return response


class GroupAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Group.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class SubjectsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Subject.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs
