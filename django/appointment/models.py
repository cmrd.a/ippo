import datetime

from django.core import validators
from django.db import models
from django.utils.formats import date_format
from django.utils.timezone import now

from people.models import Student, Teacher, Tutor
from subjects.models import Subject


class Category(models.Model):
    name = models.CharField('Название', max_length=64, unique=True)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
        ordering = ['name']

    def __str__(self):
        return self.name


class Appointment(models.Model):
    available = models.BooleanField(verbose_name='Доступна для записи', default=True)
    name = models.CharField(max_length=128, verbose_name='Название')
    teachers = models.ManyToManyField(Teacher, verbose_name='Преподаватели')
    tutors = models.ManyToManyField(Tutor, verbose_name='Тьюторы')
    start = models.DateTimeField(verbose_name='Начало', default=now)
    finish = models.TimeField(verbose_name='Конец', default=datetime.time(21, 0, 0))
    interval = models.TimeField(verbose_name='Время на одного студента', default=datetime.time(0, 0, 0),
                                help_text='Только одно из этих полей должно иметь значение, отличное от нуля')
    max = models.SmallIntegerField(verbose_name='Максимум студентов',
                                   validators=[validators.MaxValueValidator(100),
                                               validators.MinValueValidator(0)],
                                   default=30,
                                   help_text='Только одно из этих полей должно иметь значение, отличное от нуля')
    instruction = models.TextField(verbose_name='Указания', max_length=262144, blank=True)
    categories = models.ManyToManyField(Category, verbose_name='Категории')
    subjects = models.ManyToManyField(Subject, verbose_name='Предметы')

    def save(self, *args, **kwargs):
        if self.interval != datetime.time(0, 0, 0):
            self.max = 0
        super(Appointment, self).save(*args, **kwargs)

    def get_weekday(self):
        return date_format(self.start, 'l')

    def get_app_type(self):
        if self.interval != datetime.time(0, 0, 0):
            app_type = 'interval'
        else:
            app_type = 'max'
        return app_type

    def get_timelist(self):
        ts = self.start
        tf = datetime.datetime(self.start.year, self.start.month, self.start.day, self.finish.hour, self.finish.minute)
        # delta = tf-ts
        # div = delta.seconds / ((self.interval.minute * 60) + (self.interval.hour * 3600) + self.interval.second)
        sec = ((self.interval.minute * 60) + (self.interval.hour * 3600) + self.interval.second)
        strtimes = []
        while ts < tf:
            strtimes.append(ts.strftime('%H:%M'))
            ts += datetime.timedelta(seconds=sec)
        otimes = []
        for q in QueueRecord.objects.filter(appointment=self.id).values_list('time', flat=True):
            otimes.append(q)
        dates_list = [datetime.datetime.strptime(time, '%H:%M').time() for time in strtimes]
        ftimes = [item for item in dates_list if item not in otimes]
        return ftimes

    def get_left(self):
        left = 0
        if self.get_app_type() == "max":
            at_the_moment = QueueRecord.objects.filter(appointment=self.id).count()
            left = Appointment.objects.get(id=self.id).max - at_the_moment
        elif self.get_app_type() == "interval":
            left = len(self.get_timelist())
        return left

    class Meta:
        verbose_name = "Приём"
        verbose_name_plural = "Приёмы"
        ordering = ['-start']

    def __str__(self):
        return f'{self.start.strftime("%d.%m.%y %H:%M")} {self.name}'


class QueueRecord(models.Model):
    appointment = models.ForeignKey(Appointment, on_delete=models.CASCADE, verbose_name='Приём')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория')
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, verbose_name='Предмет')
    student = models.ForeignKey(Student, on_delete=models.CASCADE, verbose_name='Студент')
    created_time = models.DateTimeField(auto_now_add=True, verbose_name='Время записи')
    time = models.TimeField(verbose_name='Время в очереди')
    number = models.SmallIntegerField(verbose_name='Номер в очереди', default=0)
    MARK_CHOICES = [('b', 'Явился'), ('x', 'Не явился'), ('z', 'Зачёт'), ('n', 'Незачёт'), ('3', 'Удовл.'),
                    ('4', 'Хорошо'), ('5', 'Отлично')]
    mark = models.CharField(choices=MARK_CHOICES, max_length=1, blank=True, verbose_name='Отметка')
    remote_address = models.GenericIPAddressField(editable=False, null=True, default=None)

    class Meta:
        verbose_name = "Очередь"
        verbose_name_plural = "Очереди"
        ordering = ['time', 'number', 'created_time']
        unique_together = (("appointment", "student", "category", "subject"),)

    def __str__(self):
        return f'{self.appointment.start} {self.appointment.name}' \
               f' {self.student.group} {self.student.last_name} {self.student.first_name}'
