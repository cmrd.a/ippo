from django.contrib import admin

from .models import *


class QueueInLine(admin.TabularInline):
    model = QueueRecord
    extra = 0

    can_delete = True
    fields = ['student', 'mark', 'category', 'subject', 'number', 'time']
    readonly_fields = ['student', 'category', 'subject', 'number', 'time']


class AppointmentAdmin(admin.ModelAdmin):
    inlines = [QueueInLine]


class QueueAdmin(admin.ModelAdmin):
    list_filter = ('appointment',)
    # list_display = ('id', 'student', 'category', 'subject', 'time', 'number', 'mark')
    # list_editable = ('student', 'category', 'subject', 'mark')
    # readonly_fields = ('created_time', 'remote_address',)


admin.site.register(Category)
admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(QueueRecord, QueueAdmin)
