from django.conf.urls import url
from django.urls import path

from . import views

app_name = 'appointment'
urlpatterns = [
    path('', views.index, name="index"),
    path('<int:appointment_id>', views.appointment_details, name="appointment_details"),
    path('<int:appointment_id>/result/<str:decision>/', views.result, name="result"),
    path('<int:appointment_id>/queue_list/', views.queue_list, name="queue_list"),
    path('<int:appointment_id>/delete_from_queue_list/<int:queue_id>', views.delete_queue_record,
         name="delete_queue_record"),
    path('<int:appointment_id>/queue_list/export', views.queue_list_export, name="queue_list_export"),
    url(r'^group-autocomplete/$', views.GroupAutocomplete.as_view(), name='group-autocomplete'),
    url(r'^subjects-autocomplete/$', views.SubjectsAutocomplete.as_view(), name='subjects-autocomplete'),
    path('add/', views.edit_appointment, name="add_appointment"),
    path('edit/<int:appointment_id>/', views.edit_appointment, name="edit_appointment"),
    path('delete/<int:appointment_id>/', views.delete_appointment, name="delete_appointment"),
]
