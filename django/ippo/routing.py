import os

from channels.routing import ProtocolTypeRouter

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ippo.settings.prod')
application = ProtocolTypeRouter({
    # Empty for now (http->django views is added by default)
})
