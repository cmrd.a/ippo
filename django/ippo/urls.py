from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView
from documents.views import protected_view
from ecm.views import protected_view as ecm_view
from django.views.static import serve

urlpatterns = [
                  url(r'^favicon\.ico$', RedirectView.as_view(url='/static/main/images/ippo.ico')),
                  path('admin/', admin.site.urls),
                  path('', include('main.urls')),
                  path('diplomas/', include('diplomas.urls')),
                  path('appointments/', include('appointment.urls')),
                  path('news/', include('news.urls')),
                  url(r'^ckeditor/', include('ckeditor_uploader.urls')),
                  path('subjects/', include('subjects.urls')),
                  path('documents/', include('documents.urls')),
                  path('schedule/', include('schedule.urls')),
                  path('people/', include('people.urls')),
                  path('library/', include('library.urls')),
                  url(r'^select2/', include('django_select2.urls')),
              ] + static(
    settings.STATIC_URL,
    document_root=settings.STATIC_ROOT
) + static(
    settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT
)
#
# if settings.DEBUG:
#     urlpatterns += [
#         url(
#             r"^{}/(?P<path>.*)$".format(settings.MEDIA_URL),
#             serve,
#             {"document_root": settings.MEDIA_ROOT, "show_indexes": True}
#         ),
#         url(
#             r"^{}/(?P<path>.*)$".format(settings.PROTECTED_MEDIA_URL),
#             serve,
#             {"document_root": settings.PROTECTED_MEDIA_ROOT, "show_indexes": True}
#         ),
#     ]
# else:
urlpatterns += [
    # url(
    #     r"^{}/(?P<path>.*)$".format(settings.PROTECTED_MEDIA_URL),
    #     protected_view
    # ),
    url(r'^protected/ecm/(?P<path>.*)$', ecm_view),
    url(r'^protected/(?P<path>.*)$', protected_view),
]
