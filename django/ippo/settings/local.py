from .base import *

SECRET_KEY = "secret"
DEBUG = True
ALLOWED_HOSTS = ['*']
SITE_ID = 1

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.getenv("POSTGRES_DB"),
        "USER": os.getenv("POSTGRES_USER"),
        "PASSWORD": os.getenv("POSTGRES_PASSWORD"),
        "HOST": 'localhost',
        "PORT": 5432,
    }
}