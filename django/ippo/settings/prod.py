from .base import *
from .. import keygen

SECRET_KEY = os.getenv("SECRET_KEY", keygen.find_or_create_secret_key())

DEBUG = bool(os.getenv("DEBUG", False))

SITE_ID = 1

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.getenv("POSTGRES_DB"),
        "USER": os.getenv("POSTGRES_USER"),
        "PASSWORD": os.getenv("POSTGRES_PASSWORD"),
        "HOST": os.getenv("POSTGRES_HOST"),
        "PORT": 5432,
    }
}
