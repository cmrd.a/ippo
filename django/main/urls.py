from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name="home"),
    path('about-<str:sub_cat>', views.about, name="about"),
    path('feedback', views.feedback, name="feedback"),
    path('teacher/<int:teacher_id>', views.about_teacher, name="teacher"),
    path('worker/<int:worker_id>', views.about_worker, name="worker"),
]
