from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.core.mail import EmailMessage
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, Http404

from documents.models import Document
from news.models import Post
from people.models import Teacher, Worker
from .forms import FeedbackForm


def index(request):
    posts_list = Post.objects.filter(pub_date_time__lte=datetime.now())[:3]
    context = {
        'posts_list': posts_list,
    }
    return render(request, 'main/index.html', context)


def about(request, sub_cat):
    if sub_cat == 'today':
        today = get_object_or_404(Document, name='Кафедра сегодня').description
        return render(request, 'main/about_today.html', {'today': today})
    elif sub_cat == 'history':
        history = get_object_or_404(Document, name='История кафедры').description
        return render(request, 'main/about_history.html', {'history': history})
    elif sub_cat == 'workers':
        teachers = Teacher.objects.filter(departament__name='ИиППО')
        workers = Worker.objects.filter(departament__name='ИиППО', teacher__isnull=True)
        context = {
            'teachers': teachers,
            'workers': workers,
        }
        return render(request, 'main/about_workers.html', context)
    else:
        return Http404


def about_teacher(request, teacher_id):
    teacher = get_object_or_404(Teacher, id=teacher_id)
    subjects = teacher.subjects

    context = {
        'teacher': teacher,
    }
    return render(request, 'main/teacher.html', context)


def about_worker(request, worker_id):
    worker = get_object_or_404(Worker, id=worker_id)

    context = {
        'worker': worker,
    }
    return render(request, 'main/worker.html', context)


def feedback(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            to = [settings.EMAIL_FEEDBACK_ADDRESS]
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            email = EmailMessage(subject, message, to=to)
            email.send()
            messages.add_message(request, messages.SUCCESS, 'Сообщение отправлено')
            return HttpResponseRedirect('/')
    else:
        form = FeedbackForm()
    return render(request, 'main/feedback.html', {'form': form})
