import os
import pathlib

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.utils.functional import cached_property
from django.utils.text import slugify as django_slugify


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, **kwargs):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


# deprecated
class OverwriteStorageProtected(FileSystemStorage):
    def get_available_name(self, name, **kwargs):
        if self.exists(name):
            os.remove(os.path.join(settings.PROTECTED_MEDIA_ROOT, name))
        return name


class ProtectedFileSystemStorage(FileSystemStorage):
    @cached_property
    def base_location(self):
        return self._value_or_setting(self._location, settings.PROTECTED_MEDIA_ROOT)

    @cached_property
    def base_url(self):
        if self._base_url is not None and not self._base_url.endswith('/'):
            self._base_url += '/'
        return self._value_or_setting(self._base_url, settings.PROTECTED_MEDIA_URL)


class ecmStorage(FileSystemStorage):
    @cached_property
    def base_location(self):
        return self._value_or_setting(self._location, settings.ECM_MEDIA_ROOT)

    @cached_property
    def base_url(self):
        if self._base_url is not None and not self._base_url.endswith('/'):
            self._base_url += '/'
        return self._value_or_setting(self._base_url, settings.ECM_MEDIA_URL)


def upload_diploma(instance, file_name):
    file_name = django_slugify(instance.theme[:32], allow_unicode=True)
    file_path = os.path.join(
        'diplomas',
        str(instance.grade),
        str(instance.direction),
        str(instance.year),
        file_name,
    ) + '.pdf'

    project_folder = settings.MEDIA_ROOT
    full_path = os.path.join(project_folder, file_path)
    count = 1
    while os.path.isfile(full_path):
        file_path = os.path.join(
            'diplomas',
            str(instance.grade),
            str(instance.direction),
            str(instance.year),
            file_name,
        ) + str(count) + '.pdf'
        full_path = os.path.join(project_folder, file_path)
        count += 1
    return file_path


def upload_sched_file(instance, file_name):
    file_path = os.path.join(
        'schedule_files',
        str(instance.semester.academic_year.begin_year) + '-' + str(
            instance.semester.academic_year.end_year) + '-' + str(instance.semester.semester),
        str(instance.grade),
        str(instance.institute)
    ) + str(instance.course) + str(instance.category) + '.xlsx'
    return file_path


def upload_ecm_document(instance, file_name):
    ext = pathlib.Path(file_name).suffix.lower()
    file_path = os.path.join(
        'ecm',
        str(instance.category),
        str(instance.creation_date.strftime("%y.%m")),
        instance.code + ext
    )
    return file_path


def upload_document(instance, file_name):
    tree_path = os.path.join('doc', )
    for anc in instance.category.get_ancestors(ascending=False, include_self=True):
        name_slug = django_slugify(anc.name[:32], allow_unicode=True)
        tree_path = os.path.join(tree_path, name_slug)

    ext = pathlib.Path(file_name).suffix.lower()
    file_name = django_slugify(instance.name[:32], allow_unicode=True)
    file_path = os.path.join(
        tree_path,
        file_name + ext
    )
    project_folder = settings.PROTECTED_MEDIA_ROOT
    full_path = os.path.join(project_folder, file_path)
    count = 1
    while os.path.isfile(full_path):
        file_path = os.path.join(
            tree_path,
            file_name + str(count) + ext
        )
        full_path = os.path.join(project_folder, file_path)
        count += 1
    return file_path


def upload_load(instance, file_name):
    ext = pathlib.Path(file_name).suffix.lower()
    file_path = os.path.join(
        'loads',
        str(instance.teacher),
        str(instance.rate.academic_year.begin_year) + '-' + str(instance.rate.academic_year.end_year),
        str(instance.name)[:32] + ext
    )
    return file_path


def upload_practice(instance, file_name):
    file_name = django_slugify(instance.theme[:32], allow_unicode=True)
    file_path_without_ext = os.path.join(
        'practice',
        str(instance.category),
        str(instance.speciality.grade),
        str(instance.speciality.cipher),
        str(instance.year),
        file_name,
    )

    file_path = file_path_without_ext + '.pdf'

    project_folder = settings.MEDIA_ROOT
    full_path = os.path.join(project_folder, file_path)
    count = 1
    while os.path.isfile(full_path):
        file_path_without_ext += str(count)
        file_path = file_path_without_ext + '.pdf'
        full_path = os.path.join(project_folder, file_path)
        count += 1
    return file_path
