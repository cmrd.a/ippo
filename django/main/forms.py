from django import forms
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox


class FeedbackForm(forms.Form):
    subject = forms.ChoiceField(label='Тема', choices=[
        ('problem', 'Проблема'),
        ('suggestion', 'Предложение'),
        ('other', 'Другое')
    ], widget=forms.Select(attrs={'class': 'form-control'}))
    message = forms.CharField(label='Сообщение', max_length=1024,
                              widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}))
    captcha = ReCaptchaField(widget=ReCaptchaV2Checkbox(
        api_params={'hl': 'ru'}
    ))
