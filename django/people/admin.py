from django.contrib import admin

from .models import *


class WorkerAdmin(admin.ModelAdmin):
    search_fields = ['last_name']


class StudentInLine(admin.TabularInline):
    model = Student
    extra = 0
    show_change_link = True
    can_delete = False
    fields = ['last_name', 'first_name', 'middle_name']
    readonly_fields = ['last_name', 'first_name', 'middle_name']


class GroupAdmin(admin.ModelAdmin):
    search_fields = ['name']
    inlines = [StudentInLine]


class StudentAdmin(admin.ModelAdmin):
    search_fields = ['last_name']
    list_filter = ['group']
    autocomplete_fields = ['group']


class TeacherAdmin(admin.ModelAdmin):
    search_fields = ['last_name']
    autocomplete_fields = ['subjects', 'user']


class SubjectSemesterAdmin(admin.ModelAdmin):
    search_fields = ['subject']
    autocomplete_fields = ['subject', 'groups']


class PeriodAdmin(admin.ModelAdmin):
    list_display = ['group', 'date']
    autocomplete_fields = ['subject_semester', 'group', 'teacher']


class AcademicYearAdmin(admin.ModelAdmin):
    search_fields = ['beginning_year', 'end_year']


class LoadAdmin(admin.ModelAdmin):
    search_fields = ['name', 'teacher']
    autocomplete_fields = ['teacher']
    list_filter = ['teacher', 'rate', 'category']
    readonly_fields = ['created', 'updated']


class LoadNameAdmin(admin.ModelAdmin):
    list_filter = ['category']


# class LoadInline(admin.StackedInline):
#     model = Load
#     extra = 0


# class CustomMPTTModelAdmin(MPTTModelAdmin):
#     mptt_level_indent = 30
#     inlines = [LoadInline, ]


admin.site.register(AcademicYear, AcademicYearAdmin)
# admin.site.register(LoadCategory, CustomMPTTModelAdmin)
admin.site.register(Load, LoadAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Position)
admin.site.register(Departament)
admin.site.register(LoadName, LoadNameAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Tutor)
admin.site.register(Worker, WorkerAdmin)
admin.site.register(Semester)
admin.site.register(Rate)
admin.site.register(SubjectSemester, SubjectSemesterAdmin)
admin.site.register(Period, PeriodAdmin)
admin.site.register(Mark)
