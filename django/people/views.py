import json
import os

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Sum
from django.http import HttpResponse
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from xlrd import open_workbook

from main.models import my_slugify
from .forms import Load_2_4_Form, Load_3_Form
from .models import *


def profile(request):
    teacher = request.user.teacher
    subjects = Subject.objects.filter(teachers=teacher)

    teacher_subject_semesters = SubjectSemester.objects.filter(subject__teachers=teacher)
    current_teacher_subject_semesters = []
    for sem in teacher_subject_semesters:
        if sem.semester.now():
            current_teacher_subject_semesters.append(sem)

    periods = Period.objects.filter(teacher=teacher)
    rates = Rate.objects.filter(teachers_rates=teacher).order_by('-academic_year', 'rate')

    context = {
        'teacher': teacher,
        'subjects': subjects,
        'current_teacher_subject_semesters': current_teacher_subject_semesters,
        'periods': periods,
        'rates': rates,
    }
    return render(request, "people/profile.html", context)


def rate_loads(request, rate_id):
    teacher = request.user.teacher
    rate = get_object_or_404(Rate, pk=rate_id)
    loads = Load.objects.filter(teacher=teacher, rate=rate).order_by('category')
    ii_loads = loads.filter(category=2)
    iii_loads = loads.filter(category=3)
    iv_loads = loads.filter(category=4)
    context = {
        'ii_loads': ii_loads,
        'iii_loads': iii_loads,
        'iv_loads': iv_loads,
        'ii_pl_sum': ii_loads.aggregate(Sum('planned_labor')),
        'iii_pl_sum': iii_loads.aggregate(Sum('planned_labor')),
        'iv_pl_sum': iv_loads.aggregate(Sum('planned_labor')),
        'ii_ac_sum': ii_loads.aggregate(Sum('actual_labor')),
        'iii_ac_sum': iii_loads.aggregate(Sum('actual_labor')),
        'iv_ac_sum': iv_loads.aggregate(Sum('actual_labor')),
        'rate': rate,
    }
    return render(request, "people/rate_loads.html", context)


@login_required()
def edit_load(request, rate_id, category, load_id=None):
    teacher = request.user.teacher
    rate = get_object_or_404(Rate, pk=rate_id)
    if load_id:
        load = get_object_or_404(Load, id=load_id)
        value_name = load.name
    else:
        load = Load(teacher=teacher, rate=rate, category=category)
        value_name = ''

    if request.method == 'POST':
        if category == 2 or category == 4:
            form = Load_2_4_Form(request.POST or None, instance=load)
            # form.initial(teacher=teacher, rate=rate, category=category)
        elif category == 3:
            form = Load_3_Form(request.POST or None, instance=load)
            # form.initial(teacher=teacher, rate=rate, category=category)
        else:
            return HttpResponseNotFound('<h1>404</h1>')
        print(form.errors)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(f'/people/profile/loads/{rate.id}')
    else:
        # names = LoadName.objects.filter(category=category)
        namez = LoadName.objects.filter(category=category).values_list('name', flat=True)
        # names = json.dumps(nameszzz)
        if category == 2 or category == 4:
            form = Load_2_4_Form(instance=load)
        elif category == 3:
            form = Load_3_Form(instance=load)
        else:
            return HttpResponseNotFound('<h1>404</h1>')
        context = {
            'form': form,
            'names': json.dumps(list(namez), cls=DjangoJSONEncoder),
            'category': category,
            'rate': rate,
            'load_id': load_id,
            'value_name': value_name,
        }
        return render(request, 'people/edit_load.html', context)
    # elif category == 3:
    #     if request.method == 'POST':
    #         form = Load_3_Form(request.POST, request.FILES)
    #         print(form.errors)
    #         if form.is_valid():
    #             print(form)
    #             form.save()
    #             return HttpResponseRedirect(f'/people/profile/loads/{rate.id}')
    #     else:
    #         names = LoadName.objects.filter(category=category)
    #         form = Load_3_Form(instance=load)
    #         context = {
    #             'form': form,
    #             'names': names,
    #             'category': category,
    #             'rate': rate,
    #         }
    #         return render(request, 'people/edit_load.html', context)
    # else:
    #     return HttpResponseNotFound('<h1>404</h1>')


@login_required()
def delete_load(request, load_id):
    load = get_object_or_404(Load, pk=load_id)
    rate_id = load.rate.id
    load.delete()
    return HttpResponseRedirect(f'/people/profile/loads/{rate_id}')


def names_list(request):
    category = 2
    names = list(LoadName.objects.filter(category=category).values_list('name', flat=True))
    # json = serializers.serialize('json', names)

    # return JsonResponse(json, safe=False)
    return HttpResponse(json.dumps(names), content_type='application/json')


def semester(request, semester_id):
    teacher = request.user.teacher
    semester = get_object_or_404(SubjectSemester, pk=semester_id)
    periods = Period.objects.filter(semester=semester_id, category='пр')

    groups = semester.groups.all()
    marks = {}
    for group in groups:
        marks[group] = {}
        for student in group.student.all():
            student_str = f'{student.last_name}  {student.first_name} {student.middle_name}'
            marks[group][student_str] = {}
            for period in periods:
                if period.group.name == student.group.name:
                    mark = ''
                    try:
                        mark = Mark.objects.get(student=student, period=period).mark
                    except Mark.DoesNotExist:
                        pass
                    marks[group][student_str][period] = {'mark': mark, 'student_id': student.id, 'period_id': period.id}

    context = {
        'period_categories': Period.CATEGORY_CHOICES,
        'choices': Mark.MARK_CHOICES,
        'semester': semester,
        'periods': periods,
        'marks': marks,
    }
    return render(request, "people/semester.html", context)


def semester_table(request, semester_id, period_category):
    teacher = request.user.teacher
    semester = get_object_or_404(SubjectSemester, pk=semester_id)
    periods = Period.objects.filter(semester=semester_id, category=period_category)

    groups = semester.groups.all()
    marks = {}
    for group in groups:
        marks[group] = {}
        for student in group.student.all():
            student_str = f'{student.last_name}  {student.first_name} {student.middle_name}'
            marks[group][student_str] = {}
            for period in periods:
                if period.group.name == student.group.name:
                    mark = ''
                    try:
                        mark = Mark.objects.get(student=student, period=period).mark
                    except Mark.DoesNotExist:
                        pass
                    marks[group][student_str][period] = {'mark': mark, 'student_id': student.id, 'period_id': period.id}

    context = {
        'period_categories': Period.CATEGORY_CHOICES,
        'choices': Mark.MARK_CHOICES,
        'semester': semester,
        'periods': periods,
        'marks': marks,
    }
    return render(request, "people/semester_table.html", context)


@login_required()
def set_mark(request):
    if request.method == 'POST':
        student_id = int(request.POST['student_id'])
        period_id = int(request.POST['period_id'])
        mark = request.POST['mark']
        period = Period.objects.get(id=period_id)
        student = Student.objects.get(id=student_id)
        obj, created = Mark.objects.get_or_create(student=student, period=period)
        obj.mark = mark
        obj.save()
    return redirect(request.META.get('HTTP_REFERER'))  # todo: возврат на вкладку


@login_required()
def parse_students(request):
    folder = os.path.join(settings.MEDIA_ROOT, 'students')
    for file_name in os.listdir(folder):
        fpath = os.path.join(folder, file_name)
        wb = open_workbook(fpath)
        s = wb.sheets()[0]
        groups = Group.objects.all().values_list('name', flat=True)
        for row in range(s.nrows):
            parse_group = str(s.cell(row, 12).value)
            if parse_group in groups:
                group = Group.objects.get(name=parse_group)
                parse_name = str(s.cell(row, 0).value).split(' ')
                m_name = ''
                try:
                    m_name = parse_name[2]
                except IndexError:
                    pass

                new_student, created = Student.objects.update_or_create(group=group, last_name=parse_name[0],
                                                                        first_name=parse_name[1],
                                                                        middle_name=m_name)

    return redirect("main:home")


@login_required()
def export_loads(request):
    teacher = request.user.teacher
    academic_year = get_object_or_404(AcademicYear, begin_year=2018)
    load_queryset = Load.objects.filter(teacher=teacher, academic_year=academic_year, actual=False)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', )
    slug_last_name = my_slugify(str(teacher.last_name))
    slug_year = f'{academic_year.begin_year}-{str(academic_year.end_year)[2:]}'
    response['Content-Disposition'] = 'attachment; filename={teacher}-{date}.xlsx'.format(
        date=slug_year, teacher=slug_last_name,
    )
    workbook = Workbook()
    worksheet = workbook.active
    worksheet.title = 'loads'
    columns = [
        'name',
        'labour_intensity',
        'implementation_date',
    ]
    row_num = 1
    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title
        column_letter = get_column_letter(col_num)
        column_dimensions = worksheet.column_dimensions[column_letter]
        column_dimensions.width = 40
    for load in load_queryset:
        row_num += 1
        row = [
            load.name,
            load.labour_intensity,
            load.implementation_date,
        ]
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value
    workbook.save(response)
    return response
