// var $name_select = $('#name_select').selectize({
//     plugins: ['restore_on_backspace'],
//     sortField: 'text',
//     create: true,
// });

document.addEventListener('DOMContentLoaded', function () {
    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;
            matches = [];

            substrRegex = new RegExp(q, 'i');

            $.each(strs, function (i, str) {
                if (substrRegex.test(str))
                    matches.push(str);
            });

            cb(matches);
        }
    };


    // var names = [];
    // $.getJSON('/people/profile/load/names_list/', function (result) {
    //     $.each(result, function (i, field) {
    //         names.push(field)
    //     })
    // });

    $('#name_select').typeahead(
        {
            hint: true,
            highlight: true,
            minLength: 1,
        },
        {
            name: 'name',
            source: substringMatcher(names)
        })
});