from django import forms
from django.forms import ModelForm

from .models import Mark, Load


class MarkForm(forms.ModelForm):
    class Meta:
        model = Mark
        fields = ['period', 'student', 'mark', ]


class Load_2_4_Form(ModelForm):
    class Meta:
        model = Load
        fields = ['name', 'planned_due_date', 'actual_due_date', 'planned_labor', 'actual_labor']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'planned_labor': forms.NumberInput(attrs={'class': 'form-control', 'min': 0, 'max': 1000}),
            'actual_labor': forms.NumberInput(attrs={'class': 'form-control', 'min': 0, 'max': 1000}),
            'planned_due_date': forms.TextInput(attrs={'class': 'form-control'}),
            'actual_due_date': forms.TextInput(attrs={'class': 'form-control'}),
        }


class Load_3_Form(ModelForm):
    class Meta:
        model = Load
        fields = ['name', 'planned_due_date', 'actual_due_date', 'planned_labor', 'actual_labor', 'form', 'output_data',
                  'amount', 'coauthors', 'file', 'url']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'planned_labor': forms.NumberInput(attrs={'class': 'form-control', 'min': 0, 'max': 1000}),
            'actual_labor': forms.NumberInput(attrs={'class': 'form-control', 'min': 0, 'max': 1000}),
            'planned_due_date': forms.TextInput(attrs={'class': 'form-control'}),
            'actual_due_date': forms.TextInput(attrs={'class': 'form-control'}),
            'form': forms.Select(attrs={'class': 'form-control'}),
            'output_data': forms.Textarea(attrs={'class': 'form-control', 'rows': "3"}),
            'amount': forms.NumberInput(attrs={'class': 'form-control'}),
            'coauthors': forms.Textarea(attrs={'class': 'form-control', 'rows': "3"}),
            'file': forms.FileInput(attrs={'class': 'form-control-file'}),
            'url': forms.URLInput(attrs={'class': 'form-control', 'placeholder': 'http://'}),
            'actual': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }
