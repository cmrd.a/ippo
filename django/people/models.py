import datetime

from django.contrib.auth.models import User
from django.core import validators
from django.db import models
from django.utils import timezone
from django.utils.timezone import now
from mptt.models import MPTTModel

from main.storage import upload_load, ProtectedFileSystemStorage
from subjects.models import Subject, Speciality
from django.contrib.postgres.fields import ArrayField


class AcademicYear(models.Model):
    YEAR_CHOICES = [(r, r) for r in range(2018, timezone.now().year + 2)]
    begin_year = models.PositiveSmallIntegerField(choices=YEAR_CHOICES, default=timezone.now().year, unique=True,
                                                  verbose_name='Начало учебного года')
    end_year = models.PositiveSmallIntegerField(editable=False, verbose_name='Конец учебного года')

    class Meta:
        verbose_name = 'Учебный год'
        verbose_name_plural = 'Учебные года'
        ordering = ['begin_year']

    def __str__(self):
        return f'{self.begin_year}/{str(self.end_year)[2:]}'

    def save(self, *args, **kwargs):
        self.end_year = self.begin_year + 1
        super(AcademicYear, self).save(*args, **kwargs)

    def now(self):
        return False


class Semester(models.Model):
    academic_year = models.ForeignKey(AcademicYear, null=True, on_delete=models.CASCADE, verbose_name='Учебный год')
    semester = models.CharField(choices=[('I', 'Первый(осень)'), ('II', 'Второй(весна)')], default='I',
                                max_length=2, verbose_name='Семeстр')

    class Meta:
        verbose_name = 'Семестр'
        verbose_name_plural = 'Семестры'

    def __str__(self):
        return f'{self.academic_year} {self.get_semester_display()}'

    def now(self):
        first_begin = datetime.date(datetime.date.today().year, 9, 1)
        first_end = datetime.date(datetime.date.today().year + 1, 1, 1)
        second_begin = datetime.date(datetime.date.today().year, 1, 1)
        second_end = datetime.date(datetime.date.today().year, 9, 1)

        if first_begin < datetime.date.today() < first_end:
            if self.semester == 'I':
                return True
        elif second_begin < datetime.date.today() < second_end:
            if self.semester == 'II':
                return True

        return False


class Group(models.Model):
    name = models.CharField(max_length=12, unique=True, validators=[
        validators.RegexValidator(r'[А-Я]{4,6}-\d{2}-\d{2}')])
    speciality = models.ForeignKey(Speciality, related_name="groups", on_delete=models.SET_NULL, null=True, blank=True,
                                   verbose_name='Направление')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"
        ordering = ['name']


class Student(models.Model):
    last_name = models.CharField(max_length=128, verbose_name='Фамилия')
    first_name = models.CharField(max_length=128, verbose_name='Имя')
    middle_name = models.CharField(max_length=128, blank=True, verbose_name='Отчество')
    group = models.ForeignKey(Group, related_name='student', on_delete=models.SET_NULL, null=True,
                              verbose_name='Группа')

    def __str__(self):
        return f'{self.last_name}'

    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"
        ordering = ['last_name', 'first_name', 'middle_name']


class Departament(models.Model):
    name = models.CharField(max_length=128, unique=True, verbose_name='Название')

    class Meta:
        verbose_name = "Отдел"
        verbose_name_plural = "Отделы"
        ordering = ['name']

    def __str__(self):
        return self.name


class Position(models.Model):
    name = models.CharField(max_length=128, unique=True, verbose_name='Название')

    class Meta:
        verbose_name = "Должность"
        verbose_name_plural = "Должности"
        ordering = ['name']

    def __str__(self):
        return self.name


class Worker(models.Model):
    last_name = models.CharField(max_length=256, verbose_name='Фамилия')
    first_name = models.CharField(max_length=256, verbose_name='Имя')
    middle_name = models.CharField(max_length=256, blank=True, verbose_name='Отчество')
    departament = models.ForeignKey(Departament, on_delete=models.SET_NULL, null=True, blank=True,
                                    verbose_name='Отдел')
    position = models.ForeignKey(Position, on_delete=models.SET_NULL, null=True, blank=True,
                                 verbose_name='Должность')
    phone = models.CharField(max_length=256, null=True, blank=True, verbose_name='Номер телефона')
    email = models.EmailField(blank=True, null=True, verbose_name='Email')
    photo = models.ImageField(upload_to='worker_photo/', blank=True, null=True, verbose_name='Фото')
    birthdate = models.DateField(null=True, blank=True, verbose_name='Дата рождения')

    class Meta:
        verbose_name = "Сотрудник"
        verbose_name_plural = "Сотрудники"
        ordering = ['last_name', 'first_name', 'middle_name']
        unique_together = (("last_name", "first_name", "middle_name", "departament"),)

    def __str__(self):
        return f'{self.last_name} {self.first_name} {self.middle_name}'


class Rate(models.Model):
    academic_year = models.ForeignKey(AcademicYear, on_delete=models.CASCADE, verbose_name='Учебный год')
    rate = models.FloatField(verbose_name='Ставка')

    class Meta:
        verbose_name = "Ставка"
        verbose_name_plural = "Ставки"
        ordering = ['academic_year', 'rate']

    def __str__(self):
        return f'{self.academic_year} {self.rate}'


class Teacher(Worker):
    user = models.OneToOneField(User, related_name='teacher', on_delete=models.CASCADE, blank=True, null=True,
                                verbose_name='Учётная запись')
    subjects = models.ManyToManyField(Subject, related_name='teachers', blank=True, verbose_name='Дисциплины')
    degree = models.CharField(max_length=256, blank=True, null=True, verbose_name='Учёная степень')
    title = models.CharField(max_length=256, blank=True, null=True, verbose_name='Учёное звание')
    rates = models.ManyToManyField(Rate, related_name='teachers_rates', blank=True, verbose_name='Ставки')

    class Meta:
        verbose_name = 'Преподаватель'
        verbose_name_plural = 'Преподаватели'

    def __str__(self):
        return f'{self.last_name} {self.first_name} {self.middle_name}'


class Tutor(Student):
    user = models.OneToOneField(User, related_name='tutor', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name = 'Тьютор'
        verbose_name_plural = 'Тьюторы'

    def __str__(self):
        return f'{self.last_name} {self.first_name} {self.middle_name}'


class SubjectSemester(models.Model):
    subject = models.ForeignKey(Subject, related_name='subject_semesters', on_delete=models.CASCADE,
                                verbose_name='Дисциплина')
    groups = models.ManyToManyField(Group, verbose_name='Группы')
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE, verbose_name='Семeстр')

    class Meta:
        verbose_name = 'Семестр дисциплины'
        verbose_name_plural = 'Семестры дисциплины'

    def __str__(self):
        return f'{self.subject} {self.semester}'


class Period(models.Model):
    subject_semester = models.ForeignKey(SubjectSemester, null=True, related_name='periods', on_delete=models.CASCADE,
                                         verbose_name='Семестр')
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='period', verbose_name='Группы')
    CATEGORY_CHOICES = [('lc', 'лекция'), ('pr', 'практика'), ('lb', 'лабораторная')]
    category = models.CharField(choices=CATEGORY_CHOICES, default='lc', max_length=2, verbose_name='Тип')
    teacher = models.ForeignKey(Teacher, null=True, on_delete=models.CASCADE, verbose_name='Преподаватели')
    date = models.DateField(default=now, verbose_name='Дата')
    number = models.PositiveSmallIntegerField(verbose_name='Номер пары')

    class Meta:
        verbose_name = 'Пара'
        verbose_name_plural = 'Пары'
        ordering = ['date', 'number']

    def __str__(self):
        return f'{self.date} {self.number} {self.category}'


class Mark(models.Model):
    period = models.ForeignKey(Period, related_name='period_mark', on_delete=models.CASCADE)
    student = models.ForeignKey(Student, related_name='mark', on_delete=models.CASCADE)
    MARK_CHOICES = [('н', 'Не явился'), ('+', '+'), ('-', '-'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                    ('5', '5')]
    mark = models.CharField(choices=MARK_CHOICES, max_length=1, default='+')

    class Meta:
        verbose_name = 'Отметка'
        verbose_name_plural = 'Отметки'
        ordering = ['period', 'student']
        unique_together = [['period', 'student']]

    def __str__(self):
        return f'{self.period} {self.student} {self.mark}'


# class LoadCategory(models.Model):
#     name = models.CharField(max_length=256, verbose_name='Название')
#     actual = models.BooleanField(default=False, verbose_name='Фактическая')
#
#     class Meta:
#         verbose_name = 'Категория нагрузки'
#         verbose_name_plural = 'Категории нагрузки'
#         unique_together = ['name']
#
#     def __str__(self):
#         return self.name
#
#     def actualize(self, year, teacher):
#         if not self.actual:
#             planned_loads = self.objects.filter(loads__academic_year=year, loads__teacher=teacher)
#


class LoadName(models.Model):
    CATEGORY_CHOICES = [
        ('2', 'II. УЧЕБНО-МЕТОДИЧЕСКАЯ РАБОТА'),
        ('3', 'III. НАУЧНО-ИССЛЕДОВАТЕЛЬСКАЯ РАБОТА'),
        ('4', 'IV. ОРГАНИЗАЦИОННО-МЕТОДИЧЕСКАЯ И ВОСПИТАТЕЛЬНАЯ РАБОТА'),
    ]
    category = models.CharField(choices=CATEGORY_CHOICES, default='2', max_length=1,
                                verbose_name='Категория нагрузки')
    name = models.CharField(max_length=256, verbose_name='Наименование работы')

    class Meta:
        verbose_name = 'Имя нагрузки'
        verbose_name_plural = 'Имена нагрузок'
        ordering = ['category', 'name']

    def __str__(self):
        return self.name


class Load(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, verbose_name='Преподаватель')
    rate = models.ForeignKey(Rate, related_name='load_rate', null=True, on_delete=models.SET_NULL,
                             verbose_name='Ставка')
    CATEGORY_CHOICES = [
        ('2', 'II. УЧЕБНО-МЕТОДИЧЕСКАЯ РАБОТА'),
        ('3', 'III. НАУЧНО-ИССЛЕДОВАТЕЛЬСКАЯ РАБОТА'),
        ('4', 'IV. ОРГАНИЗАЦИОННО-МЕТОДИЧЕСКАЯ И ВОСПИТАТЕЛЬНАЯ РАБОТА'),
    ]
    category = models.CharField(choices=CATEGORY_CHOICES, default='2', max_length=1,
                                verbose_name='Категория нагрузки')
    name = models.CharField(max_length=256, verbose_name='Наименование работы')
    planned_labor = models.PositiveSmallIntegerField(verbose_name='Планируемая трудоёмкость(в часах)')
    actual_labor = models.PositiveSmallIntegerField(blank=True, null=True,
                                                    verbose_name='Фактическая трудоёмкость(в часах)')
    planned_due_date = models.CharField(max_length=32, verbose_name='Планируемый срок или период выполнения')
    actual_due_date = models.CharField(max_length=32, blank=True, null=True,
                                       verbose_name='Фактический срок или период выполнения')
    form = models.CharField(choices=[('p', 'Печатная'), ('d', 'Электронная')], max_length=1, blank=True,
                            null=True, verbose_name='Форма работы')
    output_data = models.CharField(max_length=2048, blank=True, null=True, verbose_name='Выходные данные')
    amount = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Объём(страниц)')
    coauthors = models.CharField(max_length=2048, blank=True, null=True, verbose_name='Соавторы')
    file = models.FileField(max_length=1024, upload_to=upload_load, storage=ProtectedFileSystemStorage(), null=True,
                            blank=True, verbose_name='Файл')
    url = models.URLField(null=True, blank=True, default=None, verbose_name='Ссылка')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        verbose_name = 'Преподавательская нагрузка'
        verbose_name_plural = 'Преподавательские нагрузки'
        ordering = ['category', 'name']

    def __str__(self):
        return self.name
