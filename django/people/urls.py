from django.urls import path

from . import views

app_name = 'people'
urlpatterns = [
    path('profile/', views.profile, name="profile"),
    path('profile/loads/<int:rate_id>/', views.rate_loads, name="rate_loads"),
    path('profile/loads/add/<int:rate_id>/<int:category>/', views.edit_load, name="add_load"),
    path('profile/loads/edit/<int:rate_id>/<int:category>/<int:load_id>/', views.edit_load, name="edit_load"),
    path('profile/load/delete/<int:load_id>/', views.delete_load, name="delete_load"),
    path('profile/load/names_list/', views.names_list),
    path('profile/loads/export/', views.export_loads, name="export_loads"),
    path('semester/<int:semester_id>/', views.semester, name="semester"),
    path('semester/<int:semester_id>/<str:period_category>/', views.semester_table),
    path('set_mark/', views.set_mark, name="set_mark"),
    path('parse_students/', views.parse_students, name='parse_students'),

]
