from django.urls import path

from . import views

app_name = 'subjects'

urlpatterns = [
    path('', views.index, name="index"),
    path('speciality/<int:speciality_id>', views.speciality, name="speciality"),
    path('subject/<int:subject_id>', views.subject, name="subject"),
    path('search', views.search, name="search"),
]
