from django.contrib import admin
from .models import Material, Category, Subject, Speciality


class MaterialAdmin(admin.ModelAdmin):
    search_fields = ['name']


class SubjectAdmin(admin.ModelAdmin):
    search_fields = ['name']
    autocomplete_fields = ['materials']


admin.site.register(Material, MaterialAdmin)
admin.site.register(Category)
admin.site.register(Speciality)
admin.site.register(Subject, SubjectAdmin)
