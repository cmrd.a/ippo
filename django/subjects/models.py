from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from news.models import my_slugify


class Speciality(models.Model):
    GRADE_CHOICES = [('b', 'Бакалавриат'), ('m', 'Магистратура')]
    grade = models.CharField('Степень', choices=GRADE_CHOICES, default='b', max_length=1)
    cipher = models.CharField(max_length=16, unique=True, verbose_name='Шифр')
    name = models.CharField(max_length=128, verbose_name='Название')
    description = RichTextUploadingField(null=True, blank=True, default='', verbose_name='Описание')

    class Meta:
        verbose_name = "Направление"
        verbose_name_plural = "Направления"
        ordering = ['cipher', 'name']

    def __str__(self):
        return f'{self.get_grade_display()} {self.cipher} {self.name}'


class Category(models.Model):
    name = models.CharField(max_length=128, verbose_name='Название')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
        ordering = ['name']


class Material(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория')
    part = models.PositiveSmallIntegerField(blank=True, null=True, default=None, verbose_name='Семестр')
    name = models.CharField(max_length=256, verbose_name='Имя')
    file = models.FileField(upload_to='materials/', max_length=512, verbose_name='Файл')

    class Meta:
        verbose_name = "Материал"
        verbose_name_plural = "Материалы"
        ordering = ['part', 'category', 'name']

    def __str__(self):
        return self.name


class Subject(models.Model):
    speciality = models.ForeignKey(Speciality, related_name="subjects", on_delete=models.CASCADE,
                                   verbose_name='Направление')
    name = models.CharField(max_length=256, verbose_name='Название')
    content = RichTextUploadingField(verbose_name='Описание', blank=True)
    materials = models.ManyToManyField(Material, verbose_name='Материалы', blank=True)
    slug = models.CharField(max_length=256, verbose_name='Ссылка', blank=True,
                            help_text='Сгенерируется после первого сохранения. Потом можно менять вручную')

    class Meta:
        verbose_name = "Дисциплина"
        verbose_name_plural = "Дисциплины"
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = my_slugify(self.name)
        super(Subject, self).save(*args, **kwargs)
