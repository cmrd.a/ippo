from django.shortcuts import render, get_object_or_404

from .models import Subject, Speciality


def index(request):
    specialities = Speciality.objects.all()

    context = {
        'specialities': specialities,
    }
    return render(request, 'subjects/index.html', context)


def speciality(request, speciality_id):
    specialities = Speciality.objects.all()
    current_speciality = get_object_or_404(Speciality, pk=speciality_id)
    subjects = Subject.objects.filter(speciality=current_speciality).exclude(name='[ДРУГОЕ]')
    context = {
        'specialities': specialities,
        'subjects': subjects,
        'current_speciality': current_speciality,
    }
    return render(request, 'subjects/list.html', context)


def subject(request, subject_id):
    subject = get_object_or_404(Subject, pk=subject_id)
    context = {
        'subject': subject,
    }
    return render(request, 'subjects/subject.html', context)


def search(request):
    query = request.GET.get('q')
    subjects = Subject.objects.filter(name__icontains=query) | Subject.objects.filter(content__icontains=query)
    context = {
        'subjects': subjects,
        'query': query,
    }

    return render(request, 'subjects/results.html', context)
