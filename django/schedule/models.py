from django.core.validators import FileExtensionValidator
from django.db import models

from main import storage
from people.models import Group, Semester


class ScheduleFile(models.Model):
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    GRADE_CHOICES = [('b', 'Бакалавриат'), ('m', 'Магистратура'), ('a', 'Аспирантура'), ('c', 'Колледж')]
    grade = models.CharField('Степень', choices=GRADE_CHOICES, default='b', max_length=1)
    CATEGORY_CHOICES = [('class', 'Учёба'), ('test', 'Зачёты'), ('exam', 'Экзамены')]
    category = models.CharField('Тип сессии', choices=CATEGORY_CHOICES, default='class', max_length=5)
    INSTITUTE_CHOICES = [
        ('ИИТ', 'Институт информационных технологий '),
        ('ИК', 'Институт кибернетики')
    ]
    institute = models.CharField('Институт', max_length=10)
    course = models.CharField('Курс', choices=[('1', 'Первый'), ('2', 'Второй'), ('3', 'Третий'), ('4', 'Четвёртый')],
                              default='1', max_length=1)
    file = models.FileField('Файл', null=True, upload_to=storage.upload_sched_file, storage=storage.OverwriteStorage(),
                            validators=[FileExtensionValidator(allowed_extensions=['xlsx'])], unique=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    hash_sum = models.CharField('hash', max_length=64)

    class Meta:
        ordering = ['semester', 'institute', 'grade', 'category', 'uploaded_at']

    def __str__(self):
        return f'{self.semester} {self.institute} {self.get_grade_display()} {self.course} {self.get_category_display()}'


class Room(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Teacher(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Period(models.Model):
    file = models.ForeignKey(ScheduleFile, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='sched_period_groups')
    day = models.SmallIntegerField(blank=True, null=True)
    number = models.SmallIntegerField(blank=True, null=True)
    even = models.BooleanField(blank=True, null=True)
    name = models.CharField(max_length=512, blank=True, null=True)
    category = models.CharField(max_length=32, blank=True, null=True)
    teachers = models.ManyToManyField(Teacher, related_name='sched_period_teachers', blank=True)
    rooms = models.ManyToManyField(Room, related_name='sched_period_rooms', blank=True)
    # exam fields:
    month = models.CharField(max_length=12, blank=True, null=True)
    m_day = models.CharField(max_length=12, blank=True, null=True)
    time = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        ordering = ['group', 'day', 'number', 'even']

    def __str__(self):
        return f'{self.group} {self.day} {self.number} {self.even} {self.category} {self.name} {self.str_teachers()} {self.str_rooms()} '

    def str_teachers(self):
        return '\n'.join([str(teacher) for teacher in self.teachers.all()])

    def str_rooms(self):
        return '\n'.join([str(room) for room in self.rooms.all()])
