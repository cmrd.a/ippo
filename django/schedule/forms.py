from django import forms
from .models import Room, Teacher, Period
from people.models import Group
from dal import autocomplete


class SelectScheduleForm(forms.Form):
    category = forms.ChoiceField(choices=[('odd', 'I'),
                                          ('even', 'II'),
                                          # ('test', 'Зачёты'),
                                          # ('exam', 'Экзамены')
                                          ],
                                 widget=forms.RadioSelect, required=True)
    group = forms.ModelChoiceField(queryset=Group.objects.all(), label='Группа', required=False,
                                   widget=autocomplete.ModelSelect2(url='schedule:group-autocomplete',
                                                                    attrs={'class': 'form-control',
                                                                           'onchange': 'form.submit();'}))
    teacher = forms.ModelChoiceField(queryset=Teacher.objects.all(), label='Преподаватель', required=False,
                                     widget=autocomplete.ModelSelect2(url='schedule:teacher-autocomplete',
                                                                      attrs={'class': 'form-control',
                                                                             'onchange': 'form.submit();'}))
    room = forms.ModelChoiceField(queryset=Room.objects.all(), label='Аудитория', required=False,
                                  widget=autocomplete.ModelSelect2(url='schedule:room-autocomplete',
                                                                   attrs={'class': 'form-control',
                                                                          'onchange': 'form.submit();'}))
