from django.urls import path
from django.conf.urls import url

from .views import *

app_name = 'schedule'

urlpatterns = [
    path('', index, name="index"),
    path('beta', index_beta, name="index_beta"),
    path('control', control, name="control"),
    path('parse/<int:pk>', parse, name="parse"),
    path('parse_baks/', parse_baks, name="parse_baks"),
    path('week/<str:category>/<int:idx>/<str:week_type>', week, name="week"),
    path('clear/<str:category>', clear, name="clear"),
    path('poll_state', poll_state, name="poll_state"),
    path('download', download, name="download"),
    path('identify', identify, name="identify"),
    path('celery_test', celery_test, name="celery_test"),
    url(r'^room-autocomplete/$', RoomAutocomplete.as_view(), name='room-autocomplete'),
    url(r'^teacher-autocomplete/$', TeacherAutocomplete.as_view(), name='teacher-autocomplete'),
    url(r'^group-autocomplete/$', GroupAutocomplete.as_view(), name='group-autocomplete'),
]
