import hashlib
import os
import re
from pathlib import Path

import requests
from bs4 import BeautifulSoup
from celery import shared_task
from django.conf import settings
from django.core.files.base import File
from openpyxl import load_workbook

from people.models import Group
from people.models import Semester
from .models import Period, Teacher, Room, ScheduleFile


def md5(file_path):
    hash_md5 = hashlib.md5()
    with open(file_path, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


@shared_task
def download_schedule_files():
    folder = Path(settings.MEDIA_ROOT) / 'schedule_files' / 'unidentified'
    os.makedirs(folder, exist_ok=True)
    print('\n------------------------------', folder)

    url = 'https://www.mirea.ru/education/schedule-main/schedule/'

    proxies = {
        'http': 'http://36.37.177.186:8080',

    }
    s = requests.Session()
    s.proxies = proxies
    page = s.get(url)
    soup = BeautifulSoup(page.text, "html.parser")
    links = soup.findAll('a', attrs={'href': re.compile(".+(IIT|IK)\s*.*\.(xlsx)$")})  # todo: other files?

    for idx, link in enumerate(links):
        file_name = link.get('href').split('/')[-1]
        if Path(folder / file_name).exists():
            file_name = file_name[:-5] + str(idx) + '.xlsx'
        print(idx)
        r = s.get(link.get('href'), allow_redirects=True)
        p = Path(folder / file_name)
        open(p, 'wb').write(r.content)
        # current_task.update_state(state='PROGRESS',
        #                           meta={'current': idx,
        #                                 'total': len(links)})
    return 'finished'


@shared_task
def identify_schedule_files():  # todo: check exist
    folder = Path(settings.MEDIA_ROOT) / 'schedule_files' / 'unidentified'

    amount_files = len([name for name in os.listdir(folder) if os.path.isfile(os.path.join(folder, name))])
    if amount_files == 0:
        return 'no unidentified files'

    for file_name in os.listdir(folder):
        file_path = folder / file_name
        wb = load_workbook(filename=file_path, read_only=True)
        for sheet in wb.sheetnames:
            ws = wb[sheet]
            for row in ws.iter_rows(min_row=1, max_row=2, min_col=1, max_col=4):
                for cols in row:
                    value = str(cols.value)
                    if re.match(r"\bр\s*а\s*с\s*п\s*и\s*с\s*а\s*н\s*и\s*е\b", value, re.IGNORECASE):
                        size = os.path.getsize(file_path)
                        md5_hash = md5(file_path)
                        course = 0
                        category = institute = "zero"
                        grade = "b"
                        match1 = re.search(r'\w*\d\w*', value)
                        if match1:
                            course = match1[0]
                        match2 = re.search(r'\w*занятий\w*', value)
                        if match2:
                            category = "class"
                        match2 = re.search(r'\w*зачетной\w*', value) or re.search(r'\w*зачетов\w*', value)
                        if match2:
                            category = "test"
                        match2 = re.search(r'\w*экзаменационной\w*', value)
                        if match2:
                            category = "exam"
                        match3 = re.search(r'\w*ИНТЕГУ\w*', value)
                        if match3:
                            institute = "ИНТЕГУ"
                        match3 = re.search(r'\w*КБиСП\w*', value) or re.search(r'\w*КБСП\w*', value)
                        if match3:
                            institute = "КБиСП"
                        match3 = re.search(r'\w*кибернетики\w*', value)
                        if match3:
                            institute = "ИК"
                        match3 = re.search(r'\w*\bФизико\s*-\s*технологического\w*\b', value) or re.search(
                            r'\w*ФТИ\w*', value)
                        if match3:
                            institute = "ФТИ"
                        match3 = re.search(r'\w*\bИТ\s*\w*\b', value) or re.search(
                            r'\w*\bинформационных технологий\s*\w*\b', value)
                        if match3:
                            institute = "ИТ"
                        match3 = re.search(r'\w*РТС\w*', value)
                        if match3:
                            institute = "РТС"
                        match3 = re.search(r'\w*ИЭС\w*', value)
                        if match3:
                            institute = "ИЭС"
                        match3 = re.search(r'\w*ИЭП\w*', value)
                        if match3:
                            institute = "ИЭП"
                        match3 = re.search(r'\w*ВЗО\w*', value)
                        if match3:
                            institute = "ИВЗО"
                        match3 = re.search(r'\w*ИУСТРО\w*', value)
                        if match3:
                            institute = "ИУСТРО"
                        match3 = re.search(r'\w*ТХТ\w*', value) or re.search(r'\w*тонких химических технологий\w*',
                                                                             value)
                        if match3:
                            institute = "ТХТ"

                        match4 = re.search(r'\w*магистратуры\w*', value)
                        if match4:
                            grade = "m"
                        if 'mag' in file_name:
                            grade = "m"
                        print(f'{size} {md5_hash} {course} {category} {institute} {grade}')
                        semester = Semester.objects.get(academic_year__begin_year=2019,
                                                        semester='I')  # todo: make dynamic
                        new_sched_file = ScheduleFile()
                        new_sched_file.semester = semester
                        new_sched_file.grade = grade
                        new_sched_file.category = category
                        new_sched_file.institute = institute
                        new_sched_file.course = course
                        new_sched_file.hash_sum = md5_hash
                        f = open(file_path, 'rb')
                        django_file = File(f)
                        new_sched_file.file.save(file_name, django_file)
                        new_sched_file.save()
                        f.close()
        os.remove(file_path)
    return 'identified'


@shared_task()
def parse_baks_task(baks):
    for b in baks:
        print(b)
        parse_schedule_files(b)
    print("--------------------------------baks parsed-----------------")


def get_merged_value(ws, merged_cells_ranges, col, row):
    c = ws.cell(row=row, column=col)
    result = c.value
    if type(c).__name__ != 'MergedCell':
        if result is None:
            result = ''
        return result
    result = c.value
    for mc in merged_cells_ranges:
        if ws[c.coordinate].coordinate in mc:
            result = ws.cell(row=mc.left[0][0], column=mc.left[0][1]).value
    if result is None:
        result = ''
    return result


@shared_task()
def parse_mag_schedule_files(pk):
    schedule_file = ScheduleFile.objects.get(id=pk)
    if schedule_file.grade != 'm':
        return "not magister--------------------------"
    wb = load_workbook(filename=schedule_file.file, read_only=False)
    for sheet in wb.sheetnames:
        ws = wb[sheet]
        merged_cells_ranges = ws.merged_cells.ranges
        for row in ws.iter_rows(min_row=2, max_row=3, min_col=1, max_col=256):
            for cols in row:
                if re.search(r"\w*[-]\d\d[-]\d\d", str(cols.value)):
                    y = cols.row
                    x = cols.column
                    min_row = 4
                    max_row = min_row + 141
                    min_col = x
                    max_col = min_col + 3
                    group = str(ws.cell(row=y, column=x).value)
                    print(group)
                    for idx, working_row in enumerate(
                            ws.iter_rows(
                                min_row=min_row,
                                max_row=max_row,
                                min_col=min_col,
                                max_col=max_col,
                            )
                    ):
                        new_period = Period()
                        new_period.file = schedule_file
                        try:
                            new_period.group = Group.objects.get(name=group)
                        except Group.DoesNotExist:
                            new_group = Group(name=group)
                            new_group.save()
                            new_period.group = new_group
                        day = idx // 26 + 1
                        new_period.day = day
                        number = get_merged_value(ws, merged_cells_ranges, 2, idx + min_row)

                        n70 = list(range(14, 20))
                        for i in range(5):
                            n70 += [j + 26 for j in n70]
                        n70 = set(n70)
                        n80 = [i + 6 for i in n70]
                        if idx in n70:
                            number = 8
                        elif idx in n80:
                            number = 9
                        new_period.number = number
                        even = get_merged_value(ws, merged_cells_ranges, 5, idx + min_row)
                        if even == 'I':
                            even = 0
                        elif even == 'II':
                            even = 1
                        new_period.even = even
                        name = get_merged_value(ws, merged_cells_ranges, working_row[0].column, working_row[0].row)
                        new_period.name = name
                        category = get_merged_value(ws, merged_cells_ranges, working_row[1].column, working_row[1].row)
                        new_period.category = category
                        teacher = get_merged_value(ws, merged_cells_ranges, working_row[2].column, working_row[2].row)
                        if teacher != '':
                            new_period.save()
                            Teacher.objects.get_or_create(name=teacher)
                            new_period.teachers.add(Teacher.objects.get(name=teacher))

                        room = get_merged_value(ws, merged_cells_ranges, working_row[3].column, working_row[3].row)
                        if room != '':
                            new_period.save()
                            Room.objects.get_or_create(name=room)
                            new_period.rooms.add(Room.objects.get(name=room))

                        if name != '':
                            new_period.save()
                            print(day, number, even, category, '\t', teacher, '\t', room, '\t', name)


@shared_task()
def parse_schedule_files(pk):
    obj = ScheduleFile.objects.get(id=pk)
    if obj.grade != 'b':
        return "not bachelor--------------------------"
    cat = obj.category
    wb = load_workbook(filename=obj.file, read_only=True)

    if cat == 'class' or cat == 'test':
        for sheet in wb.sheetnames:
            ws = wb[sheet]
            for row in ws.iter_rows(min_row=2, max_row=3, min_col=1, max_col=256):
                for cols in row:
                    if re.search(r"\w*[-]\d\d[-]\d\d", str(cols.value)):
                        y = cols.row
                        x = cols.column
                        min_row = 4
                        max_row = min_row + 71
                        min_col = x
                        max_col = min_col + 3

                        group = str(ws.cell(row=y, column=x).value)
                        print(group)
                        number = 1
                        for idx, working_row in enumerate(
                                ws.iter_rows(
                                    min_row=min_row,
                                    max_row=max_row,
                                    min_col=min_col,
                                    max_col=max_col,
                                )
                        ):
                            new_period = Period()
                            new_period.file = obj
                            try:
                                new_period.group = Group.objects.get(name=group)
                            except Group.DoesNotExist:
                                new_group = Group(name=group)
                                new_group.save()
                                new_period.group = new_group

                            day = idx // 12 + 1
                            new_period.day = day
                            if number > 6:
                                number = 1
                            new_period.number = number
                            if idx % 2 == 0:
                                even = 0
                            else:
                                even = 1
                            new_period.even = even
                            if working_row[0].value is None:
                                name = ""
                            else:
                                name = str(working_row[0].value)
                            # name = str(os.linesep.join([s for s in name.splitlines() if s]))
                            new_period.name = name
                            if working_row[1].value is None:
                                new_period.category = ""
                            else:
                                new_period.category = working_row[1].value

                            # --------------------teacher-----------------
                            c_value = str(working_row[2].value).split("\n")
                            if c_value[0] != "None":
                                first_t_name = str(c_value[0]).strip()
                                new_period.save()
                                Teacher.objects.get_or_create(name=first_t_name)
                                new_period.teachers.add(
                                    Teacher.objects.get(name=first_t_name)
                                )
                            try:
                                second_t_name = str(c_value[2]).strip()
                                new_period.save()
                                Teacher.objects.get_or_create(name=second_t_name)
                                new_period.teachers.add(
                                    Teacher.objects.get(name=second_t_name)
                                )
                            except IndexError:
                                pass

                            # --------------------room-----------------
                            c_value = str(working_row[3].value).split("\n")
                            if c_value[0] != "None":
                                first_r_name = c_value[0]
                                new_period.save()
                                Room.objects.get_or_create(name=first_r_name)
                                new_period.rooms.add(Room.objects.get(name=first_r_name))
                            try:
                                second_r_name = c_value[1]
                                new_period.save()
                                Room.objects.get_or_create(name=second_r_name)
                                new_period.rooms.add(Room.objects.get(name=second_r_name))
                            except IndexError:
                                pass
                            # -------------------end--------------------
                            number += even
                            if (
                                    working_row[0].value
                                    and working_row[0].value != "День"
                                    and working_row[0].value != "самостоятельных"
                                    and working_row[0].value != "занятий"
                            ):
                                new_period.save()

    elif cat == 'exam':
        for sheet in wb.sheetnames:
            ws = wb[sheet]
            month = str(ws.cell(row=3, column=2).value).replace(" ", "")
            for row in ws.iter_rows(min_row=2, max_row=3, min_col=0, max_col=120):
                for cols in row:
                    if re.search(r"\w*[-]\d\d[-]\d\d", str(cols.value)):
                        group = str(cols.value)
                        print(group)
                        y = cols.row
                        x = cols.column
                        min_row = y + 1
                        max_row = 100
                        min_col = x - 1
                        max_col = min_col + 3

                        for working_row in (ws.iter_rows(
                                min_row=min_row,
                                max_row=max_row,
                                min_col=min_col,
                                max_col=max_col,
                        )
                        ):
                            if str(working_row[1].value).strip() == 'Консультация' or str(
                                    working_row[1].value).strip() == 'Экзамен':
                                category = working_row[1].value
                                m_day = str(ws.cell(row=working_row[1].row, column=3).value).replace('\n', ' ').strip()
                                name = str(ws.cell(row=working_row[1].row + 1, column=working_row[1].column).value)
                                teacher = str(ws.cell(row=working_row[1].row + 2, column=working_row[1].column).value)
                                time = str(working_row[2].value)
                                room = str(working_row[3].value).replace('\n', ' ').strip()
                                # print(working_row[1].value, m_day, time, room, name, teacher)

                                new_period = Period()
                                new_period.file = obj
                                try:
                                    new_period.group = Group.objects.get(name=group)
                                except Group.DoesNotExist:
                                    new_group = Group(name=group)
                                    new_group.save()
                                    new_period.group = new_group

                                new_period.category = category
                                new_period.month = month
                                new_period.m_day = m_day
                                new_period.name = name
                                new_period.time = time
                                new_period.save()
                                if teacher != 'None':
                                    Teacher.objects.get_or_create(name=teacher)
                                    new_period.teachers.add(Teacher.objects.get(name=teacher))
                                if room != 'None':
                                    Room.objects.get_or_create(name=room)
                                    new_period.rooms.add(Room.objects.get(name=room))
                                new_period.save()

    print(obj, '-------------------------------------------parsing complete----------------------------')
