var $selector = $('#selector').selectize({
    lockOptgroupOrder: true,
    // searchField: ['value'],
    plugins: ['optgroup_columns', 'remove_button'],
    sortField: 'text',
    onChange: function (value) {
        show_week(value)
    }
});

const switcher = document.getElementById('switcher');

switcher.addEventListener('change', (event) => {
    show_week($selector[0].value);
});

// todo: change even to type
function show_week(value) {
    let words = value.split('-');
    let category = words[0];
    let idx = words[1];
    let ev = $('input[name=inlineRadioOptions]:checked').val();
    if (idx !== undefined) {
        $('#out').load('week/' + category + '/' + idx + '/' + ev)
    }
}

