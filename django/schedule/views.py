import json
import shutil
from datetime import timedelta, date
from pathlib import Path

from celery.result import AsyncResult
from dal import autocomplete
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

from people.models import Group
from schedule.tasks import download_schedule_files, identify_schedule_files, parse_schedule_files, parse_baks_task, \
    parse_mag_schedule_files
from .models import ScheduleFile, Period, Teacher, Room
from .forms import SelectScheduleForm


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


def index(request):
    start_date = date(2019, 9, 1)
    end_date = date(2019, 12, 31)
    week_number = ''
    for single_date in daterange(start_date, end_date):
        if date.today() == single_date:
            # print(f'{single_date.strftime("%Y.%m.%d")} {single_date.isocalendar()[1] - 6}')
            week_number = single_date.isocalendar()[1] - 35

    if not week_number:
        week_number = 99

    group_list = Group.objects.filter(sched_period_groups__isnull=False)
    teacher_list = Teacher.objects.filter(sched_period_teachers__isnull=False)
    room_list = Room.objects.filter(sched_period_rooms__isnull=False)

    updated = ScheduleFile.objects.first().uploaded_at.strftime("%d.%m.%y")

    context = {
        "group_list": group_list,
        "teacher_list": teacher_list,
        "room_list": room_list,
        "week_number": week_number,
        "updated": updated
    }
    return render(request, "schedule/index.html", context)


def index_beta(request):
    if request.method == 'POST':
        form = SelectScheduleForm(request.POST)
        if form.is_valid():
            category = form.data.get('category')
            group = form.cleaned_data.get('group')
            teacher = form.cleaned_data.get('teacher')
            room = form.cleaned_data.get('room')
            periods = {}
            if group:
                if category == 'odd':
                    even = 0
                else:
                    even = 1
                periods = Period.objects.filter(file__category='class', even=even, group__name=group)
                periods = periods.values_list('day', 'number', 'name', 'teachers__name', 'rooms__name')

            context = {
                'form': SelectScheduleForm({'category': 'even'}),
                'periods': periods,
                'group': group,
                'category': category,
            }
            return render(request, "schedule/index_beta.html", context)
    else:
        form = SelectScheduleForm({'category': 'even'})
    context = {
        'form': form,
    }
    return render(request, "schedule/index_beta.html", context)


@login_required
def control(request):
    file_list = ScheduleFile.objects.all().order_by('category', 'institute', 'grade', 'course')
    # file_list = ScheduleFile.objects.all()
    context = {"file_list": file_list}
    return render(request, "schedule/control.html", context)


@login_required
def clear(request, category):
    # Teacher.objects.all().delete()
    # Room.objects.all().delete()
    if category == 'files':
        ScheduleFile.objects.all().delete()
        folder = Path(settings.MEDIA_ROOT) / 'schedule_files'
        try:
            shutil.rmtree(folder)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))
    elif category == 'periods':
        Period.objects.all().delete()
    return redirect("/schedule/control")


def celery_test(request):
    res = download_schedule_files.delay()

    return redirect("/schedule/control")


def poll_state(request):
    if 'job' in request.GET:
        job_id = request.GET['job']
    else:
        return HttpResponse('No job id given.')
    job = AsyncResult(job_id)
    data = job.result or job.state
    return HttpResponse(json.dumps(data), content_type='application/json')


@login_required
def download(request):
    download_schedule_files.delay()
    # job = download_schedule_files.delay()
    # # return HttpResponseRedirect(reverse('schedule:poll_state') + '?job=' + job.id)
    # context = {
    #     'job_id': job.id,
    # }
    return render(request, "schedule/control.html")


@login_required
def identify(request):
    identify_schedule_files.delay()
    return HttpResponse('wait 1 minute and continue')


# todo: refactor
def week(request, category, idx, week_type):
    print(week_type)
    if week_type == 'exam':
        period_quuryset = Period.objects.filter(file__category='exam').order_by('m_day', 'time')
        if category == "g":
            p = period_quuryset.filter(group=idx)
        elif category == "t":
            p = period_quuryset.filter(teachers__id=idx)
        elif category == "r":
            p = period_quuryset.filter(rooms__id=idx)
        else:
            p = False
        if p:
            periods = p

            context = {
                "periods": periods,
                "category": category,
                "week_type": week_type,
                "upd_date": period_quuryset[0].file.uploaded_at.strftime("%d.%m.%y"),
            }
            return render(request, "schedule/week.html", context)
    elif week_type == 'test':
        periods = {}
        for d in range(1, 7):
            periods[d] = {}
            for n in range(1, 7):
                periods[d][n] = {}
                if category == "g":
                    p = Period.objects.filter(file__category='test', day=d, number=n, group=idx)
                elif category == "t":
                    p = Period.objects.filter(file__category='test', day=d, number=n, teachers__id=idx)
                elif category == "r":
                    p = Period.objects.filter(file__category='test', day=d, number=n, rooms__id=idx)
                else:
                    p = False
                if p:
                    if category == "t" or category == "r":
                        group_list = list(p.values_list("group__name", flat=True))
                        periods[d][n]["groups"] = " ".join([str(gr) for gr in group_list])
                        p = p.order_by("day", "number").distinct("day", "number")
                    name_list = list(p.values_list("name", flat=True))
                    periods[d][n]["name"] = " ".join([str(name) for name in name_list])
                    category_list = list(p.values_list("category", flat=True))
                    periods[d][n]["category"] = " ".join(
                        [str(cat) for cat in category_list]
                    )
                    room_list = list(p.values_list("rooms__name", flat=True))
                    periods[d][n]["room"] = " ".join([str(room) for room in room_list])
                    if periods[d][n]["room"] == "None":
                        periods[d][n]["room"] = ""
                    teacher_list = list(p.values_list("teachers__name", flat=True))
                    periods[d][n]["teacher"] = " ".join(
                        [str(teacher) for teacher in teacher_list]
                    )
                    if periods[d][n]["teacher"] == "None":
                        periods[d][n]["teacher"] = ""
        context = {
            "periods": periods,
            "category": category
        }
        return render(request, "schedule/week.html", context)
    elif week_type == 'odd' or week_type == 'even':
        template = "schedule/week.html"
        if week_type == 'odd':
            even = 0
        else:
            even = 1
        periods = {}
        for d in range(1, 7):
            periods[d] = {}
            max_numbers = 7
            if category == "g":
                temp = Period.objects.filter(file__category='class', group=idx)
                grade = temp.first().file.grade
                if grade == 'm':
                    max_numbers = 10
                    template = "schedule/week_mag_class.html"

            for n in range(1, max_numbers):
                periods[d][n] = {}
                if category == "g":
                    p = Period.objects.filter(file__category='class', day=d, number=n, even=even, group=idx)
                elif category == "t":
                    p = Period.objects.filter(file__category='class', day=d, number=n, even=even, teachers__id=idx)
                elif category == "r":
                    p = Period.objects.filter(file__category='class', day=d, number=n, even=even, rooms__id=idx)
                else:
                    p = False
                if p:
                    if category == "t" or category == "r":
                        group_list = list(p.values_list("group__name", flat=True))
                        periods[d][n]["groups"] = " ".join([str(gr) for gr in group_list])
                        p = p.order_by("day", "number").distinct("day", "number")
                    name_list = list(p.values_list("name", flat=True))
                    periods[d][n]["name"] = " ".join([str(name) + '\n' for name in name_list])
                    category_list = list(p.values_list("category", flat=True))
                    periods[d][n]["category"] = " ".join(
                        [str(cat) + '\n' for cat in category_list]
                    )
                    room_list = list(p.values_list("rooms__name", flat=True))
                    periods[d][n]["room"] = " ".join([str(room) + '\n' for room in room_list])
                    if periods[d][n]["room"] == "None":
                        periods[d][n]["room"] = ""
                    teacher_list = list(p.values_list("teachers__name", flat=True))
                    periods[d][n]["teacher"] = " ".join(
                        [str(teacher) + '\n' for teacher in teacher_list]
                    )
                    if periods[d][n]["teacher"] == "None":
                        periods[d][n]["teacher"] = ""
        context = {
            "periods": periods,
            "category": category
        }
        return render(request, template, context)


@login_required
def parse(request, pk):
    obj = ScheduleFile.objects.get(id=pk)
    grade = obj.grade
    if grade == 'b':
        parse_schedule_files.delay(pk)
        # print("------------------------baks", grade ,obj.file)
    elif grade == 'm':
        parse_mag_schedule_files.delay(pk)
        # print("------------------------mags",grade,obj.file)
    return HttpResponse('work kicked off!')


@login_required
def parse_baks(request):
    files = ScheduleFile.objects.filter(grade="b")
    baks = list(files.values_list("id", flat=True))
    print(baks)
    parse_baks_task.delay(baks)
    return HttpResponse('wait 10 minutes and continue')


class GroupAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Group.objects.exclude(sched_period_groups__isnull=True)
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class TeacherAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Teacher.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class RoomAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Room.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs
