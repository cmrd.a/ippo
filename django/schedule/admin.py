from django.contrib import admin

from .models import ScheduleFile, Period, Teacher, Room


class PeriodAdmin(admin.ModelAdmin):
    list_filter = ['teachers']


admin.site.register(ScheduleFile)
admin.site.register(Period)
admin.site.register(Teacher)
admin.site.register(Room)
