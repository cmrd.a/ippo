$('#id_tag').selectize({
    plugins: ['restore_on_backspace', 'remove_button'],
    delimiter: ',',
    persist: true,
    maxItems: 10,
    options: [
        "tag1",
        "tag2",
    ],
    create: function (input) {
        return {
            value: input,
            text: input
        }
    }
});


var mytoggler = document.getElementsByClassName("box");

for (let i = 0; i < mytoggler.length; i++) {
    mytoggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("check-box");
    });
}

function m_is_toggle() {
    var x = document.getElementById("m_is");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}