from django.urls import path
from . import views

app_name = 'diplomas'

urlpatterns = [
    path('', views.index, name='index'),
    path('speciality/<int:speciality_id>', views.speciality, name='speciality'),
    path('tag/<int:tag_id>', views.tag, name='tag'),
    path('search', views.search, name="search"),
]
