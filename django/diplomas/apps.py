from django.apps import AppConfig


class DiplomasConfig(AppConfig):
    name = 'diplomas'
    verbose_name = "Библиотека ВКР"
