from django.shortcuts import render, get_object_or_404

from subjects.models import Speciality
from .models import Diploma, Tag


def index(request):
    specialities = Speciality.objects.all()

    context = {
        'specialities': specialities,
    }
    return render(request, 'diplomas/index.html', context)


def speciality(request, speciality_id):
    current_speciality = get_object_or_404(Speciality, pk=speciality_id)
    diplomas = Diploma.objects.filter(speciality=current_speciality)
    years = diplomas.order_by('year').distinct('year').values_list('year', flat=True)

    context = {
        'diplomas': diplomas,
        'current_speciality': current_speciality,
        'years': years,
    }
    return render(request, 'diplomas/list.html', context)


def tag(request, tag_id):
    diplomas = Diploma.objects.filter(tag=tag_id)
    tag = get_object_or_404(Tag, pk=tag_id)
    years = diplomas.order_by('year').distinct('year').values_list('year', flat=True)
    context = {
        'diplomas': diplomas,
        'tag': tag,
        'years': years,
    }
    return render(request, 'diplomas/tag.html', context)


def search(request):
    query = request.GET.get('q')
    diplomas = Diploma.objects.filter(theme__icontains=query) | Diploma.objects.filter(abstract__icontains=query)
    years = diplomas.order_by('year').distinct('year').values_list('year', flat=True)
    context = {
        'diplomas': diplomas,
        'query': query,
        'years': years,
    }

    return render(request, 'diplomas/results.html', context)
