from django.db import models
import datetime
from django.utils import timezone
from django.core.validators import FileExtensionValidator
from main import storage
from subjects.models import Speciality


class Tag(models.Model):
    name = models.CharField(max_length=45, unique=True, verbose_name='Тег', help_text='Максимум 45 символов')

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"
        ordering = ['name']

    def __str__(self):
        return self.name


class Diploma(models.Model):
    GRADE_CHOICES = [('b', 'Бакалавриат'), ('m', 'Магистратура')]
    grade = models.CharField('Степень', choices=GRADE_CHOICES, default='b', max_length=4)
    DIRECTION_CHOICES = [('pi', 'Программная инженерия'), ('is', 'Информационные системы и технологии')]
    direction = models.CharField(choices=DIRECTION_CHOICES, default='pi', max_length=4)
    speciality = models.ForeignKey(Speciality, blank=True, null=True, on_delete=models.CASCADE,
                                   verbose_name='Направление')
    YEAR_CHOICES = [(r, r) for r in range(2010, timezone.now().year + 1)]
    year = models.IntegerField('Год', choices=YEAR_CHOICES, default=timezone.now().year)
    theme = models.CharField('Тема', max_length=1024)
    abstract = models.TextField('Аннотация', max_length=262144)
    file = models.FileField('Файл', upload_to=storage.upload_diploma, storage=storage.OverwriteStorage(),
                            validators=[FileExtensionValidator(allowed_extensions=['pdf'])])
    uploaded_at = models.DateTimeField(auto_now_add=True)
    tag = models.ManyToManyField(Tag, blank=True, verbose_name='Теги')

    class Meta:
        verbose_name = "ВКР"
        verbose_name_plural = "ВКРы"
        ordering = ['theme']

    def __str__(self):
        return str(self.year) + ' ' + self.theme

    # def get_absolute_url(self):
    #     return reverse('diplomas:detail', args=[str(self.id)])
