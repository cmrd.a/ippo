from django.contrib import admin
from .models import Diploma, Tag
from django.db import models
from ckeditor.widgets import CKEditorWidget


# class RelInline(admin.TabularInline):
#     model = Diploma.tag.through


class TagAdmin(admin.ModelAdmin):
    #    inlines = [RelInline, ]
    ordering = ['name']
    search_fields = ['name']


class DiplomaAdmin(admin.ModelAdmin):
    list_filter = ['direction', 'grade', 'year']
    #   inlines = [RelInline, ]
    search_fields = ['name', 'abstract']
    # exclude = ('tag', )
    autocomplete_fields = ['tag']
    formfield_overrides = {models.TextField: {'widget': CKEditorWidget}}


admin.site.register(Diploma, DiplomaAdmin)
admin.site.register(Tag, TagAdmin)
