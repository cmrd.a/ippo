import django_filters
from django import forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404

from people.models import Speciality
from .models import Practice, Tag


class PracticeFilter(django_filters.FilterSet):
    category = django_filters.ChoiceFilter(choices=Practice.CATEGORY_CHOICES,
                                           widget=forms.Select(attrs={'class': 'form-control'}))
    year = django_filters.ChoiceFilter(choices=Practice.YEAR_CHOICES,
                                       widget=forms.Select(attrs={'class': 'form-control'}))
    speciality = django_filters.ModelChoiceFilter(queryset=Speciality.objects.all(),
                                                  widget=forms.Select(attrs={'class': 'form-control'}))


def index(request):
    practice_filter = PracticeFilter(request.GET, queryset=Practice.objects.all())

    paginator = Paginator(practice_filter.qs, 10)
    page = request.GET.get('page')

    try:
        practices = paginator.page(page)
    except PageNotAnInteger:
        practices = paginator.page(1)
    except EmptyPage:
        practices = paginator.page(paginator.num_pages)

    return render(request, 'library/index.html', {
        'practices': practices,
        'page': page,
        'filter': practice_filter,
    })


def works_with_tags(request, tag_id):
    tag = get_object_or_404(Tag, pk=tag_id)
    qs = Practice.objects.filter(tags=tag)

    paginator = Paginator(qs, 10)
    page = request.GET.get('page')

    try:
        practices = paginator.page(page)
    except PageNotAnInteger:
        practices = paginator.page(1)
    except EmptyPage:
        practices = paginator.page(paginator.num_pages)

    return render(request, 'library/works_with_tags.html', {
        'practices': practices,
        'page': page,
        'qs': qs,
        'title': f'Работы с тегом: {tag.name}'
    })
