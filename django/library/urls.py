from django.urls import path
from . import views

app_name = 'library'

urlpatterns = [
    path('', views.index, name='index'),
    path('tag/<int:tag_id>', views.works_with_tags, name='works_with_tags'),
]
