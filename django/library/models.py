from django.db import models
from django.utils import timezone
from django.core.validators import FileExtensionValidator
from main import storage
from subjects.models import Speciality
from diplomas.models import Tag


class Practice(models.Model):
    CATEGORY_CHOICES = [
        ('уч_озн',
         'Учебная: Ознакомительная практика'),
        ('уч_перв',
         'Учебная: Практика по получению первичных профессиональных умений...'),
        ('пр_тех',
         'Производственная: Технологическая (проектно-технологическая) практика'),
        ('пр_проект',
         'Производственная: Проектная практика'),
        ('пр_проф',
         'Производственная: Практика по получению профессиональных умений...'),
        ('пр_пред',
         'Производственная: Преддипломная практика'),
        ('нир',
         'НИР: Научно-исследовательская работа'),
    ]
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=10, verbose_name='Вид')

    speciality = models.ForeignKey(Speciality, on_delete=models.CASCADE, verbose_name='Направление')
    YEAR_CHOICES = [(y, y) for y in range(2016, timezone.now().year + 1)]
    year = models.IntegerField('Год', choices=YEAR_CHOICES, default=timezone.now().year)
    theme = models.CharField('Тема', max_length=1024)
    abstract = models.TextField('Аннотация', max_length=262144)
    file = models.FileField('Файл', upload_to=storage.upload_practice, storage=storage.OverwriteStorage(),
                            validators=[FileExtensionValidator(allowed_extensions=['pdf'])])
    uploaded_at = models.DateTimeField(auto_now_add=True)
    tags = models.ManyToManyField(Tag, blank=True, verbose_name='Теги')

    class Meta:
        verbose_name = "Практика"
        verbose_name_plural = "Практики"
        ordering = ['speciality', 'year', 'theme']

    def __str__(self):
        return self.theme
