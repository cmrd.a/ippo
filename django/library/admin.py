from django.contrib import admin
from .models import Practice
from django.db import models
from ckeditor.widgets import CKEditorWidget


class PracticeAdmin(admin.ModelAdmin):
    autocomplete_fields = ['tags']
    list_filter = ['speciality', 'year', 'category']
    formfield_overrides = {models.TextField: {'widget': CKEditorWidget}}


admin.site.register(Practice, PracticeAdmin)
