ubf:
	docker-compose up --build --force-recreate
udb:
	docker-compose up -d --build
ub:
	docker-compose up --build
u:
	docker-compose up
d:
	docker-compose down
dv:
	docker-compose down -v
cs:
	docker-compose run --rm django /bin/bash -c 'python manage.py collectstatic --no-input'

mm:
	docker-compose run --rm django /bin/bash -c 'python manage.py makemigrations'

m:
	docker-compose run --rm django /bin/bash -c 'python manage.py migrate'

bash:
	docker-compose run --rm django /bin/bash -c 'bash'

flush:
	docker-compose down
	docker system prune -a
	docker volume prune

prod:
	docker-compose -f docker-compose.prod.yml up -d --build

proddown:
	docker-compose -f docker-compose.prod.yml down

prodmm:
	docker-compose -f docker-compose.prod.yml run --rm django /bin/bash -c 'bash'

prodmm:
	docker-compose -f docker-compose.prod.yml run --rm django /bin/bash -c 'python manage.py makemigrations'

prodm:
	docker-compose -f docker-compose.prod.yml run --rm django /bin/bash -c 'python manage.py migrate'

ls:
	docker-compose exec pgadmin python setup.py --load-servers servers.json --user admin@sutula.ya

prodl:
	docker-compose -f docker-compose.prod.yml logs -f

prodr:
	docker-compose -f docker-compose.prod.yml restart